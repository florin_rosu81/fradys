#ifndef __CONVOLUTION_H__
#define __CONVOLUTION_H__

#include "fractional.h"

/**
 * \brief Compute the nth element of the convolution between f and g.
 *
 * \param[in] f     An array of size at least n.
 * \param[in] g     An array of size at least n.
 * \param[in] n     The element of the convolution to return.
 *
 * \return The result of (f * g)[n].
 */
double convolution_element(double *f, double *g, size_t n);

/**
 * \brief Compute the convolution between two arrays.
 *
 * We will compute:
 *      h[n] = \sum_{i = 0}^n f[n - i] * b[i]
 * for all n. Normally, the linear convolution between an array f of size N and
 * an array g of size M is of size N + M - 1. In our case f and g are the same
 * size and we only compute half of the convolution since the other half
 * is symmetric.
 *
 * \param[out] h    The result of the convolution.
 */
void convolution_seq(double *f, double *g, double *h, size_t N);

/**
 * \brief Perform exchange between processes for the FFT-based convolution.
 *
 * In our very specific case, we need to compute the convolution between
 * an array f that doesn't change and an array g that does. We suppose that
 * the f array has been correctly initialized, so we need to exchange the
 * elements of g only.
 *
 * To perform a convolution with the help of the FFT, we need to do the
 * following:
 *      (F * G) = IFFT(FFT(F) .* FFT(G))
 * where F and G are constructed from the arrays f and g that are padded
 * by 0 to the size 2 * N of the resulting convolution. Since the f array
 * does not change, we can construct it padded from the beginning, but for
 * the g array, we have to pad it every time.
 *
 * The padding of the array consists of moving all the data from the processes
 * over mpisize / 2 to all those under. This way half the array will contain
 * the actual data and half will contain only zeros.
 *
 * If, for some reason, you want to pad both arrays, call the function with
 * exchange_all = 1.
 *
 * \sa fractional_data_t, convolution_fftw().
 */
void convolution_fftw_exchange(fractional_data_t * data, MPI_Comm mpicomm,
                               int exchange_all);

/**
 * \brief Compute the convolution using FFTW.
 *
 * This function computes the convolution between the f and g members of
 * the data struct  and stores the result in the h memeber. Before calling
 * this function, if the convolution is not circular, one should call
 * convolution_fftw_exchange to make sure all the data is in the right place
 * before computing the FFT.
 *
 * \param[in] exchange      Boolean to indicate whether to exchange the result.
 */
void convolution_fftw(fractional_data_t * data, MPI_Comm mpicomm, int exchange);

#endif /* __CONVOLUTION_H__ */
