#ifndef __INTERPOLATION_H__
#define __INTERPOLATION_H__

#include <stdlib.h>

typedef enum {
#ifdef HAVE_GSL
  INTERP_GSL_LINEAR,
  INTERP_GSL_POLYNOMIAL,
  INTERP_GSL_SPLINE_CUBIC,
  INTERP_GSL_AKIMA,
#endif
  INTERP_POLYNOMIAL,
  INTERP_SPLINE_CUBIC
} interpolation_type_t;

void interpolate(double *T, double *Y, size_t N, double *t, double *y, size_t n,
                 interpolation_type_t type);

#endif                          /* __INTERPOLATION_H__ */
