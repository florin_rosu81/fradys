#ifndef __FRACTIONAL_H__
#define __FRACTIONAL_H__

#include "utils.h"

/**
 * \brief Type of variable.
 */
typedef double variable_t;

/**
 * \brief Prototype for the right hand side.
 */
typedef variable_t(*function_t) (double t, variable_t y, double alpha);

/**
 * \brief Local mesh data.
 *
 * This structure contains information about the variables and coefficients
 * on a specific process. The global information about the variables and
 * coefficients is distributed among all the processes, each of them holding
 * only a slice
 *          [n * p, n * (p + 1)]
 * where n is the number of time steps per process.
 *
 * It also contains information necessary to perform the global FFTW
 * convolutions. We compute the convolution using the convolution theorem:
 *      f * g = DFFT^{-1}[DFFT(f) . DFFT(g)]
 * where . is the element-wise multiplication. In practice this is achieved
 * by:
 *      fhat = DFFT(f)
 *      ghat = DFFT(g)
 *      hhat_i = fhat_i * ghat_i for all i
 *      h = DFFT^{-1}(hhat)
 *      return h
 *
 * In our case, f will be one of the coefficients a or b and g will be
 * the array of function calls f(t_i, y_i).
 */
typedef struct {
  /* Mesh information */
  size_t n;             /**< number of time steps on current process */
  size_t offset;        /**< starting iteration for current process */
  double *t;            /**< All the time points */
  variable_t *y;        /**< variables on current process */
  variable_t *yp;       /**< array of predictors in the ABM scheme */

  /* Cache for coefficients */
  double *a;
  double *b;
  double *c;

  /* FFTW information */
  fftw_plan forward;
  fftw_plan backward;

  /* FFTW extra variables */
  /* NOTE: FFTW doesn't do 1d real transforms with MPI, so they're complex */
  fftw_complex *f;
  fftw_complex *fhat;
  fftw_complex *g;
  fftw_complex *ghat;
  fftw_complex *h;
  fftw_complex *hhat;
} fractional_data_t;

/**
 * \brief Struct describing the data necessary for the parareal algorithm.
 */
typedef struct {
  MPI_Comm mpicomm;
  int mpisize;
  int mpirank;
  int cached;       /**< 1 if the cache arrays in fractional_data_t are initialized */

  function_t f;     /**< right hand side */
  double alpha;     /**< order of the derivative */
  double t0;        /**< start time */
  double y0;        /**< initial condition at t0 */
  double f0;        /**< f(t0, y0) */

  fractional_data_t coarse;
  fractional_data_t fine;
} fractional_t;

/**
 * \brief Allocate the variables and set the initial condition.
 *
 * Available initial condition ids are:
 *      mittang_leffler         f(t, y) = -y and y(t_0) = E_alpha(t_0)
 *
 * \param[in] id    The id of the initial condition.
 * \param[in] N     The number of coarse steps on a process.
 * \param[in] n     The number of fine steps on a process.
 * \param[in] t0    The time of the initial condition.
 * \param[in] alpha The order of the derivative.
 * \return A fully allocated struct.
 */
fractional_t *fractional_new(const char *id, size_t N, size_t n,
                             double dt, double dT, double t0,
                             double alpha, MPI_Comm mpicomm);

/**
 * \brief Free the data.
 */
void fractional_destroy(fractional_t * data);

/**
 * \brief Precompute coefficients
 *
 * The cache is given by:
 *      a_j = ((j + 2)^(alpha + 1) -
 *             2 (j + 1)^(alpha + 1) + j^(alpha + 1)) / Gamma(alpha + 2)
 *      b_j = ((j + 1)^alpha - j^alpha) / Gamma(alpha + 1)
 *      c_j = (j^(alpha + 1) - (j - alpha)(j + 1)^alpha) / Gamma(alpha + 2)
 *
 * Each process only computes the coefficients for
 *      j \in [n * p, n * (p + 1)]
 *
 * Sets cached = 1 in fractional_t.
 *
 * \param[in] frac      Global information about the MPI partitions.
 */
void fractional_cache_init(fractional_t * data);

/**
 * \brief Perform the parareal iterations.
 *
 * We are trying to solve the fractional differential equation:
 *      D^alpha y(t) = f(t, y(t)).
 * for an order alpha in [0, 1] and by using a predictor-corrector scheme
 * (Adams-Bashforth-Moulton):
 *
 *  y^P_n = y_0 + dt^alpha \sum_{j = 0}^{n - 1} b_{n - j - 1} f(t_j, y^C_j).
 *  y^C_n = y_0 + dt^alpha (c_{n - 1} f(t_0, y_0) +
 *                               \sum_{j = 1}^{n - 1} a_{n - j - 1} f(t_j, y^C_j) +
 *                               f(t_n, y^P_n) / Gamma(alpha + 2))
 *
 * \param[in,out] data      The info for the fractional parareal algorithm.
 * \param[in] t0            Starting time
 * \param[in] dT            Coarse time step.
 * \param[in] dt            Fine time step.
 * \param[in] kmax          Maximum number of parareal iterations.
 */
void fractional_parareal(fractional_t * data, double dt, double dT,
                         size_t kmax);

#endif                          /* __FRACTIONAL_H__ */
