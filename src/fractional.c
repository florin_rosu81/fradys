#include "fractional.h"
#include "interpolation.h"
#include "convolution.h"

/****************************************************************************
 *                      Initial Conditions
 ****************************************************************************/
static variable_t mittag_leffler_f(double t, variable_t y, double alpha)
{
  UNUSED(t);
  UNUSED(alpha);

  return -y;
}

static variable_t mittag_leffler_y0(double t)
{
  UNUSED(t);

  return 1.0;
}

/****************************************************************************
 *                      Helper Functions
 ****************************************************************************/
static void fractional_coefficients_init(double *a, double *b, double *c,
                                         size_t start, size_t end, double alpha)
{
  const double gamma0 = tgamma(alpha + 1);
  const double gamma1 = tgamma(alpha + 2);

  for (size_t j = start, n = 0; j < end; ++j, ++n) {
    a[n] = (pow(j + 2, alpha + 1) -
            2 * pow(j + 1, alpha + 1) + pow(j, alpha + 1)) / gamma1;
    b[n] = (pow(j + 1, alpha) - pow(j, alpha)) / gamma0;
    c[n] = (pow(j, alpha + 1) - (j - alpha) * pow(j + 1, alpha)) / gamma1;
  }
}

static void fractional_data_init(fractional_data_t * data, size_t n,
                                 double t0, double dt, MPI_Comm mpicomm)
{
  int mpisize;
  int mpirank;
  ptrdiff_t N;
  ptrdiff_t ni, no, so;

  MPI_Comm_size(mpicomm, &mpisize);
  MPI_Comm_rank(mpicomm, &mpirank);

  /* Get sizes from FFTW */
  N = 2 * n * mpisize;
  data->n = fftw_mpi_local_size_1d(N, mpicomm, FFTW_FORWARD, FFTW_ESTIMATE,
                                   &ni, (ptrdiff_t *) & (data->offset), &no,
                                   &so);
  CHECK_ABORT(data->n == (size_t) ni, "Weird. This is weird.. isn't it weird?");

  /* allocate */
  data->t = utils_malloc(data->n * sizeof(double));
  data->y = utils_malloc(data->n * sizeof(double));
  data->yp = utils_malloc(data->n * sizeof(double));

  data->f = utils_fftw_malloc(2 * data->n * sizeof(fftw_complex));
  data->fhat = utils_fftw_malloc(2 * data->n * sizeof(fftw_complex));
  data->g = utils_fftw_malloc(2 * data->n * sizeof(fftw_complex));
  data->ghat = utils_fftw_malloc(2 * data->n * sizeof(fftw_complex));
  data->h = utils_fftw_malloc(2 * data->n * sizeof(fftw_complex));
  data->hhat = utils_fftw_malloc(2 * data->n * sizeof(fftw_complex));

  /* create plans */
  data->forward = fftw_mpi_plan_dft_1d(N, data->f, data->fhat, mpicomm,
                                       FFTW_FORWARD, FFTW_ESTIMATE);
  data->backward = fftw_mpi_plan_dft_1d(N, data->hhat, data->h, mpicomm,
                                        FFTW_BACKWARD, FFTW_ESTIMATE);

  /* init time */
  for (size_t i = data->offset; i < (data->offset + data->n); ++i) {
    data->t[i] = t0 + i * dt;
  }
}

static void fractional_data_reset(fractional_data_t * data, int cached)
{
  /* free main data arrays */
  utils_free(data->t);
  utils_free(data->y);
  utils_free(data->yp);
  utils_fftw_free(data->f);
  utils_fftw_free(data->fhat);
  utils_fftw_free(data->g);
  utils_fftw_free(data->ghat);
  utils_fftw_free(data->h);
  utils_fftw_free(data->hhat);

  /* free coefficients, if they have been allocated */
  if (cached) {
    utils_free(data->a);
    utils_free(data->b);
    utils_free(data->c);
  }
}

/****************************************************************************
 *                      Scheme Functions
 ****************************************************************************/
static void fractional_abm_sequential(function_t f, variable_t * Y, size_t N,
                                      double alpha, double t0, double y0,
                                      double dt)
{
  const double h = pow(dt, alpha);
  const double gamma1 = tgamma(alpha + 2);

  variable_t *fY = NULL;
  double *a = NULL;
  double *b = NULL;
  double *c = NULL;

  double Yp = 0;
  double t = 0;
  double conv = 0;

  /* allocate */
  fY = utils_malloc(N * sizeof(variable_t));
  a = utils_malloc(N * sizeof(double));
  b = utils_malloc(N * sizeof(double));
  c = utils_malloc(N * sizeof(double));

  /* init coefficients */
  fractional_coefficients_init(a, b, c, 0, N, alpha);
  Y[0] = y0;
  fY[0] = f(t0, y0, alpha);

  /* scheme */
  for (size_t n = 1; n < N; ++n) {
    t = t0 + n * dt;

    /* predictor */
    conv = convolution_element(b, fY, n);
    Yp = Y[0] + h * conv;

    /* corrector */
    conv = convolution_element(a, fY, n) - a[n - 1] * fY[0];
    Y[n] = Y[0] + h * (c[n - 1] * fY[0] + conv + f(t, Yp, alpha) / gamma1);
    fY[n] = f(t, Y[n], alpha);
  }

  utils_free(fY);
  utils_free(a);
  utils_free(b);
  utils_free(c);
}

static void fractional_abm(fractional_data_t * data, function_t f, double y0,
                           double alpha, double dt, MPI_Comm mpicomm)
{
  const double gamma1 = tgamma(alpha + 2);
  const double h = pow(dt, alpha);
  const double f0 = f(data->t[0], y0, alpha);
  double conv = 0;

  /* perform the convolution b * f */
  for (size_t j = 0; j < data->n; ++j) {
    data->f[j] = data->b[j];
    data->g[j] = f(data->t[j], data->y[j], alpha);
  }
  /* exchange the values of g before doing the convolution */
  convolution_fftw_exchange(data, mpicomm, 0);
  convolution_fftw(data, mpicomm, 1);

  /* compute the predictors */
  for (size_t j = 1; j < data->n; ++j) {
    data->yp[j] = y0 + h * creal(data->h[j - 1]);
  }

  /* perform the convolution a * f */
  for (size_t j = 0; j < data->n; ++j) {
    data->f[j] = data->a[j];
  }
  convolution_fftw(data, mpicomm, 1);

  /* compute the correctors */
  for (size_t j = 1; j < data->n; ++j) {
    conv = data->h[j - 1] - data->a[j - 1] * f0;
    data->y[j] = y0 + h * (data->c[j - 1] * f0 + conv +
                           f(data->t[j], data->yp[j], alpha) / gamma1);
  }
}

static void fractional_parareal_init(fractional_t * data, double dt)
{
  int mpirank = data->mpirank;
  MPI_Status status;

  size_t n = data->coarse.n;
  size_t N = n * data->mpisize;
  variable_t *y = data->coarse.y;

  if (mpirank == 0) {
    /* sequentially compute the solution on the coarse grid */
    variable_t *Y = utils_malloc(N * sizeof(variable_t));
    fractional_abm_sequential(data->f, Y, N, data->alpha, data->t0, data->y0,
                              dt);

    /* copy the solution on the root */
    memcpy(y, Y, n * sizeof(variable_t));

    /* send the rest of the solution to each processor */
    for (int p = 1; p < data->mpisize; ++p) {
      MPI_Send(Y + p * n, n, MPI_DOUBLE, 0, 42, data->mpicomm);
    }

    utils_free(Y);
  } else {
    MPI_Recv(y, n, MPI_DOUBLE, 0, 42, data->mpicomm, &status);
  }

  /* interpolate the coarse to the fine solution */
  interpolate(data->coarse.t, y, n, data->fine.t, data->fine.y, data->fine.n,
              INTERP_GSL_SPLINE_CUBIC);
}

/****************************************************************************
 *                      Public Functions
 ****************************************************************************/
fractional_t *fractional_new(const char *id, size_t N, size_t n,
                             double dT, double dt, double t0,
                             double alpha, MPI_Comm mpicomm)
{
  fractional_t *data = utils_calloc(1, sizeof(fractional_t));

  /* save the MPI info */
  data->mpicomm = mpicomm;
  MPI_Comm_size(mpicomm, &(data->mpisize));
  MPI_Comm_rank(mpicomm, &(data->mpirank));

  /* initial conditions */
  if (strcmp(id, "mittag_leffler") == 0) {
    data->f = mittag_leffler_f;
    data->y0 = mittag_leffler_y0(t0);
  } else {
    ABORT("Invalid id for the initial condition: \"%s\".", id);
  }

  /* save the other info as well */
  data->alpha = alpha;
  data->t0 = t0;
  data->f0 = data->f(t0, data->y0, alpha);
  fractional_data_init(&(data->coarse), N, t0, dT, mpicomm);
  fractional_data_init(&(data->fine), n, t0, dt, mpicomm);

  return data;
}

void fractional_destroy(fractional_t * data)
{
  fractional_data_reset(&(data->coarse), data->cached);
  fractional_data_reset(&(data->fine), data->cached);

  utils_free(data);
}

void fractional_cache_init(fractional_t * data)
{
  fractional_data_t *coarse = &(data->coarse);
  fractional_data_t *fine = &(data->fine);

  /* initialize coarse */
  coarse->a = utils_malloc(coarse->n * sizeof(double));
  coarse->b = utils_malloc(coarse->n * sizeof(double));
  coarse->c = utils_malloc(coarse->n * sizeof(double));
  fractional_coefficients_init(coarse->a, coarse->b, coarse->c,
                               coarse->offset, coarse->offset + coarse->n,
                               data->alpha);

  /* initialize fine */
  fine->a = utils_malloc(fine->n * sizeof(double));
  fine->b = utils_malloc(fine->n * sizeof(double));
  fine->c = utils_malloc(fine->n * sizeof(double));
  fractional_coefficients_init(fine->a, fine->b, fine->c,
                               fine->offset, fine->offset + fine->n,
                               data->alpha);

  data->cached = 1;
}

void fractional_parareal(fractional_t * data, double dt, double dT, size_t kmax)
{
  fractional_data_t *coarse = &(data->coarse);
  fractional_data_t *fine = &(data->fine);
  variable_t *Yk;
  variable_t *yk;

  Yk = utils_malloc(coarse->n * sizeof(variable_t));
  yk = utils_malloc(fine->n * sizeof(variable_t));

  /* compute the coarse solution sequentially */
  fractional_parareal_init(data, dT);
  for (size_t n = 0; n < coarse->n; ++n) {
    Yk[n] = coarse->y[n];
  }

  /* iterate */
  for (size_t k = 0; k < kmax; ++k) {
    /* integrate on the fine grid on the current interval */
    fractional_abm(fine, data->f, data->y0, data->alpha, dt, data->mpicomm);

    /* integrate on the coarse grid with values interpolate from the fine */
    interpolate(fine->t, fine->y, fine->n, coarse->t, coarse->y, coarse->n,
                INTERP_GSL_SPLINE_CUBIC);
    fractional_abm(coarse, data->f, data->y0, data->alpha, dT, data->mpicomm);

    /* correct the fine solution: y^{k + 1} = Y^{k + 1} + y^k - Y^k */
    interpolate(coarse->t, coarse->y, coarse->n, fine->t, yk, fine->n,
                INTERP_GSL_SPLINE_CUBIC);
    for (size_t n = 0; n < fine->n; ++n) {
      fine->y[n] += yk[n];
    }

    interpolate(coarse->t, Yk, coarse->n, fine->t, yk, fine->n,
                INTERP_GSL_SPLINE_CUBIC);
    for (size_t n = 0; n < fine->n; ++n) {
      fine->y[n] -= yk[n];
    }

    /* save the previous coarse value */
    for (size_t n = 0; n < coarse->n; ++n) {
      Yk[n] = coarse->y[n];
    }
  }

  utils_free(Yk);
  utils_free(yk);
}
