#include "convolution.h"

static void convolution_fftw_exchange_in (fftw_complex * f, ptrdiff_t n,
                                          MPI_Comm mpicomm)
{
    int mpisize;
    int mpirank;
    MPI_Status status;

    MPI_Comm_size(mpicomm, &mpisize);
    MPI_Comm_rank(mpicomm, &mpirank);

    /* send this half to its rightful process */
    if (mpirank != 0) {
        MPI_Send(f, n, MPI_C_DOUBLE_COMPLEX, mpirank / 2, 42, mpicomm);
    }

    /* receive two halves from other processes */
    if (mpirank >= (mpisize / 2)) {
        memset (f, 0, 2 * n * sizeof(fftw_complex));
    } else {
        if (mpirank != 0) {
            MPI_Recv(f, n, MPI_C_DOUBLE_COMPLEX, 2 * mpirank, 42, mpicomm, &status);
        }
        MPI_Recv(f + n, n, MPI_C_DOUBLE_COMPLEX, 2 * mpirank + 1, 42, mpicomm, &status);
    }
}

static void convolution_fftw_exchange_out(fftw_complex * f, ptrdiff_t n,
                                          MPI_Comm mpicomm)
{
    int mpisize;
    int mpirank;
    MPI_Status status;

    MPI_Comm_size(mpicomm, &mpisize);
    MPI_Comm_rank(mpicomm, &mpirank);

    if (mpirank < (mpisize / 2)) {
        if (mpirank != 0) {
            MPI_Send(f, n, MPI_C_DOUBLE_COMPLEX, 2 * mpirank, 43, mpicomm);
        }
        MPI_Send(f + n, n, MPI_C_DOUBLE_COMPLEX, 2 * mpirank + 1, 43, mpicomm);
    }

    if (mpirank != 0) {
        MPI_Recv(f, n, MPI_C_DOUBLE_COMPLEX, mpirank / 2, 43, mpicomm, &status);
    }
}

double convolution_element(double *f, double *g, size_t n)
{
  double sum = 0;

#pragma omp parallel for reduction(+:sum)
  for (size_t j = 0; j < n; ++j) {
    sum += f[n - j - 1] * g[j];
  }

  return sum;
}

void convolution_seq(double *f, double *g, double *h, size_t N)
{
#pragma omp parallel for
    for (size_t n = 1; n < N + 1; ++n) {
        h[n - 1] = 0;
        for (size_t j = 0; j < n; ++j) {
            h[n - 1] += f[n - j - 1] * g[j];
        }
    }
}

void convolution_fftw_exchange (fractional_data_t * data, MPI_Comm mpicomm,
                                int exchange)
{
    convolution_fftw_exchange_in(data->g, data->n, mpicomm);
    if(exchange) {
        convolution_fftw_exchange_in(data->f, data->n, mpicomm);
    }
}

void convolution_fftw (fractional_data_t * data, MPI_Comm mpicomm, int exchange)
{
  fftw_mpi_execute_dft(data->forward, data->f, data->fhat);
  fftw_mpi_execute_dft(data->forward, data->g, data->ghat);

#pragma omp parallel for
  for (size_t i = 0; i < data->n; ++i) {
    data->hhat[i] = data->fhat[i] * data->ghat[i];
  }

  fftw_mpi_execute_dft(data->backward, data->hhat, data->h);

  /* send the first half to the process that requires it */
  if (exchange) {
    convolution_fftw_exchange_out(data->h, data->n, mpicomm);
  }
}
