#include "interpolation.h"
#include "utils.h"

#ifdef HAVE_GSL
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#endif

static void interpolate_lagrange(double *T, double *Y, size_t N,
                                 double *t, double *y, size_t n)
{
  double prod = 1.0;

  for (size_t k = 0; k < n; ++k) {
    y[k] = 0.0;

    for (size_t i = 0; i < N; ++i) {
      prod = 1.0;

      for (size_t j = 0; j < i; ++j) {
        prod *= (t[k] - T[j]) / (T[i] - T[j]);
      }
      for (size_t j = i + 1; j < N; ++j) {
        prod *= (t[k] - T[j]) / (T[i] - T[j]);
      }

      y[k] += prod * Y[i];
    }
  }
}

static void interpolate_spline_cubic(double *T, double *Y, size_t N,
                                     double *t, double *y, size_t n)
{
  UNUSED(T);
  UNUSED(Y);
  UNUSED(N);
  UNUSED(t);
  UNUSED(y);
  UNUSED(n);

  ABORT("Not implmemented.");
}

#ifdef HAVE_GSL
static void interpolate_gsl(double *T, double *Y, size_t N,
                            double *t, double *y, size_t n,
                            const gsl_interp_type * type)
{
  gsl_interp_accel *acc = gsl_interp_accel_alloc();
  gsl_spline *interp = gsl_spline_alloc(type, N);

  gsl_spline_init(interp, T, Y, N);

  for (size_t i = 0; i < n; ++i) {
    y[i] = gsl_spline_eval(interp, t[i], acc);
  }
  gsl_spline_free(interp);
  gsl_interp_accel_free(acc);
}
#endif

void interpolate(double *T, double *Y, size_t N, double *t, double *y, size_t n,
                 interpolation_type_t type)
{
  switch (type) {
    case INTERP_POLYNOMIAL:
      interpolate_lagrange(T, Y, N, t, y, n);
      break;
    case INTERP_SPLINE_CUBIC:
      interpolate_spline_cubic(T, Y, N, t, y, n);
      break;
#ifdef HAVE_GSL
    case INTERP_GSL_LINEAR:
      interpolate_gsl(T, Y, N, t, y, n, gsl_interp_linear);
      break;
    case INTERP_GSL_POLYNOMIAL:
      interpolate_gsl(T, Y, N, t, y, n, gsl_interp_polynomial);
      break;
    case INTERP_GSL_SPLINE_CUBIC:
      interpolate_gsl(T, Y, N, t, y, n, gsl_interp_cspline);
      break;
    case INTERP_GSL_AKIMA:
      interpolate_gsl(T, Y, N, t, y, n, gsl_interp_akima);
      break;
#endif
    default:
      LOG_GLOBAL("Unknown interpolation method.\n");
  }
}
