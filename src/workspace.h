#ifndef __WORKSPACE_H__
#define __WORKSPACE_H__

#include "fractional.h"

/**
 * \brief Main structure that contains the state of the simulation.
 */
typedef struct {
  MPI_Comm mpicomm;
  int mpisize;
  int mpirank;

  char *filename;       /**< Output filename. */
  char *ic;             /**< Initial condition. */
  double t0, tn;        /**< Time interval. */
  double alpha;         /**< Order of the derivative. */

  /**< Parareal Coarse */
  size_t N;             /**< Iterations per process. */
  double dT;            /**< Time step. */

  /**< Parareal Fine */
  size_t n;             /**< Iterations per process. */
  double dt;            /**< Time step. */

  fractional_t *data;
} workspace_t;

/**
 * \brief Initialize a new workspace.
 */
workspace_t *workspace_new(MPI_Comm mpicomm, int argc, char **argv);

/**
 * \brief Free all the allocated data.
 */
void workspace_destroy(workspace_t * w);

/**
 * \brief Compute the solution at all time steps.
 */
void workspace_solve(workspace_t * w);

/**
 * \brief Save the solution to the given file.
 *
 * The solution is saved in pairs (t_n, y(t_n)). Can be opened in gnuplot or
 * others.
 */
void workspace_save(workspace_t * w);

#endif                          /* __WORKSPACE_H__ */
