#include "utils.h"

static int utils_identifier = -1;
static int utils_malloc_count = 0;
static int utils_using_threads = 0;

void utils_init(MPI_Comm mpicomm, int *argc, char ***argv)
{
  int mpiret = 0;
  utils_malloc_count = 0;

  MPI_Init_thread(argc, argv, MPI_THREAD_FUNNELED, &utils_using_threads);
  mpiret = MPI_Comm_rank(mpicomm, &utils_identifier);
  CHECK_MPI(mpiret);

  if (utils_using_threads >= MPI_THREAD_FUNNELED) {
    utils_using_threads = fftw_init_threads();
  }
  if (utils_using_threads) {
    fftw_plan_with_nthreads(4);
  }

  fftw_mpi_init();
}

void utils_finalize(void)
{
  int mpiret;

  if (utils_malloc_count != 0) {
    LOG_WARNING("Uneven malloc / free calls.");
  }

  if (utils_using_threads) {
    fftw_cleanup_threads();
  }
  fftw_mpi_cleanup();
  mpiret = MPI_Finalize();
  CHECK_MPI(mpiret);
}

void *utils_malloc(size_t size)
{
  void *p = malloc(size);
  CHECK_MEMORY(p);

  ++utils_malloc_count;
  return p;
}

void *utils_fftw_malloc(size_t size)
{
  void *p = fftw_malloc(size);
  CHECK_MEMORY(p);

  ++utils_malloc_count;
  return p;
}

void *utils_calloc(size_t n, size_t size)
{
  void *p = calloc(n, size);
  CHECK_MEMORY(p);

  ++utils_malloc_count;
  return p;
}

char *utils_strdup(const char *str)
{
  char *p = utils_malloc(strlen(str) + 1);
  strcpy(p, str);

  return p;
}

void utils_free(void *p)
{
  --utils_malloc_count;
  free(p);
}

void utils_fftw_free(void *p)
{
  --utils_malloc_count;
  fftw_free(p);
}

int utils_is_root(void)
{
  return utils_identifier <= 0;
}
