#include "workspace.h"

#include <time.h>

int main(int argc, char **argv)
{
  MPI_Comm mpicomm = MPI_COMM_WORLD;

  workspace_t *w = NULL;
  double time = 0;

  /* initialize */
  utils_init(mpicomm, &argc, &argv);

  /* create a new workspace */
  w = workspace_new(mpicomm, argc, argv);

  /* solve our fractional differential equation */
  time = (double) clock();
  workspace_solve(w);
  time = ((double) clock() - time) / CLOCKS_PER_SEC;
  printf("Elapsed time: %g\n", time);

  /* write everything to a file */
  workspace_save(w);

  /* destroy the workspace */
  workspace_destroy(w);

  /* finalize everything */
  utils_finalize();

  return EXIT_SUCCESS;
}
