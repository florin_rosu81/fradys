#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <math.h>
#include <complex.h>
#include <fftw3-mpi.h>

#define UNUSED(x) ((void)(x))
#define FUZZYZERO(x) (fabs (x) < 1e-12)
#define MIN(a, b) ((a) > (b) ? (b) : (a))

/**
 * \brief Initialize the utils for use with MPI.
 */
void utils_init(MPI_Comm mpicomm, int *argc, char ***argv);

/**
 * \brief Finalize the utils for use with MPI.
 */
void utils_finalize(void);

/**
 * \brief Malloc the memory and exit on failure.
 */
void *utils_malloc(size_t size);

/**
 * \brief Allocate a complex FFTW array.
 */
void *utils_fftw_malloc(size_t size);


/**
 * \brief Calloc the memory and exit on failure.
 */
void *utils_calloc(size_t n, size_t size);

/**
 * \brief Free.
 */
void utils_free(void *p);

/**
 * \brief Free FFTW array.
 */
void utils_fftw_free(void *p);

/**
 * \brief strdup implementation since it is not part of the standard.
 *
 * TODO: add checks to see if it's available.
 */
char *utils_strdup(const char *str);

/**
 * \brief Check whether the current process is the root (0).
 */
int utils_is_root(void);

/**
 * \brief Debug Macros.
 */
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define __FUNC__ __func__

#define clean_errno() (errno == 0 ? "None" : strerror(errno))

/*
 * Define special variadic macros to make gcc happy when using c99.
 * The problem is that if we use these macros directly, __VA_ARGS__ can be
 * empty, but the c99 standard doesn't like that.
 */
#define LOGC99(M, ...) fprintf(stderr, M "%s\n", __VA_ARGS__)
#define LOG_DEBUGC99(M, ...) fprintf(stderr, "DEBUG %s:%s:%d: " M "%s\n", \
                                 __FILENAME__, __FUNC__, __LINE__, __VA_ARGS__)
#define LOG_ERRORC99(M, ...) fprintf(stderr, "[ERROR] (%s:%s:%d: errno: %s) " M "%s\n", \
                                     __FILENAME__, __FUNC__, __LINE__, clean_errno(), __VA_ARGS__)
#define LOG_WARNINGC99(M, ...) fprintf(stderr, "[WARN] (%s:%s:%d: errno: %s) " M "%s\n", \
                                       __FILENAME__, __FUNC__, __LINE__, clean_errno(), __VA_ARGS__)
#define LOG_INFOC99(M, ...) fprintf(stderr, "[INFO] (%s:%s:%d) " M "%s\n", \
                                    __FILENAME__, __FUNC__, __LINE__, __VA_ARGS__)
#define ABORTC99(M, ...) \
    do { LOG_ERRORC99(M, __VA_ARGS__); exit(EXIT_FAILURE); } while(0)

#ifdef NDEBUG
#define LOG_DEBUG(...)
#define LOG_DEBUG_GLOBAL(...)
#else
#define LOG_DEBUG(...) LOG_DEBUGC99(__VA_ARGS__, "")
#define LOG_DEBUG_GLOBAL(...) \
    do { (utils_is_root() ? LOG_DEBUGC99(__VA_ARGS__, ""): 0); } while(0)
#endif

#define LOG(...) LOGC99(__VA_ARGS__, "")
#define LOG_ERROR(...) LOG_ERRORC99(__VA_ARGS__, "")
#define LOG_WARNING(...) LOG_WARNINGC99(__VA_ARGS__, "")
#define LOG_INFO(...) LOG_INFOC99(__VA_ARGS__, "")

#define LOG_GLOBAL(...) \
    do { (utils_is_root () ? LOGC99(__VA_ARGS__, "") : 0); } while (0)

#define ABORT(...) ABORTC99(__VA_ARGS__, "")
#define CHECK_ABORT(A, ...) if(!(A)) { ABORTC99(__VA_ARGS__, ""); }
#define CHECK_MEMORY(A) CHECK_ABORT((A), "Out of memory.")
#define CHECK_MPI(A) CHECK_ABORT((A) == MPI_SUCCESS, "MPI Error.")

#endif                          /* __UTILS_H__ */
