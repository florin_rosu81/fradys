#include "workspace.h"

#include <getopt.h>

static struct option options[] = {
  {"filename", required_argument, 0, 'f'},
  {"initial", required_argument, 0, 'i'},
  {"t0", required_argument, 0, 't'},
  {"tn", required_argument, 0, 'T'},
  {"alpha", required_argument, 0, 'a'},
  {"coarse", required_argument, 0, 'N'},
  {"fine", required_argument, 0, 'n'},
  {"help", no_argument, 0, 'h'},
  {0, 0, 0, 0}
};

static void usage()
{
  printf("Usage: fractional [OPTIONS] [-h].\n");
  printf("\nOptions:\n");
  printf("\t-f, --filename     Filename to save solution\n");
  printf("\t-i, --initial      Name of the initial condition\n");
  printf("\t-t, --t0           Starting time\n");
  printf("\t-T, --tn           Ending time\n");
  printf("\t-a, --alpha        Order of the derivative\n");
  printf("\t-N, --coarse       Coarse number of points per process\n");
  printf("\t-n, --fine         Fine number of points per process\n");
  printf("\t-h, --help         Display help message\n");
  printf("\nTHIS PROGRAM COMES WITH ABSOLUTELY NO WARRANTY.\n");
}

static void workspace_handle_options(workspace_t * w, int argc, char **argv)
{
  int c = 0;
  int option_index = 0;

  /* set default values */
  w->filename = NULL;
  w->ic = NULL;
  w->t0 = 0.0;
  w->tn = 1.0;
  w->alpha = 0.5;
  w->N = w->mpisize;
  w->n = 100;

  while (1) {
    c = getopt_long(argc, argv, "f:n:t:T:a:h", options, &option_index);
    if (c == -1) {
      break;
    }

    switch (c) {
      case 'f':
        w->filename = utils_strdup(optarg);
        break;
      case 'i':
        w->ic = utils_strdup(optarg);
        break;
      case 't':
        w->t0 = atof(optarg);
        break;
      case 'T':
        w->tn = atof(optarg);
        break;
      case 'a':
        w->alpha = atof(optarg);
        break;
      case 'N':
        w->N = atol(optarg);
        break;
      case 'n':
        w->n = atol(optarg);
        break;
      case 'h':
        usage();
        exit(0);
      case '?':
        break;
      default:
        LOG_DEBUG_GLOBAL("Unrecognized option: %d.\n", c);
    }
  }

  /* if no filename has been given, use a default one */
  if (w->filename == NULL) {
    w->filename = utils_strdup("fractional.dat");
  }

  /* if no initial condition has been given, use a default one */
  if (w->ic == NULL) {
    w->ic = utils_strdup("mittag_leffler");
  }

  /* compute the time steps */
  w->N = (w->N <= 1 ? 2 : w->N);
  w->n = (w->n <= 1 ? 2 : w->n);
  w->dT = (w->tn - w->t0) / ((w->N - 1) * w->mpisize);
  w->dt = (w->tn - w->t0) / ((w->n - 1) * w->mpisize);

  LOG_DEBUG_GLOBAL("processes:  %d", w->mpisize);
  LOG_DEBUG_GLOBAL("filename:   \"%s\"", w->filename);
  LOG_DEBUG_GLOBAL("ic:         \"%s\"", w->ic);
  LOG_DEBUG_GLOBAL("time:       [%g, %g]", w->t0, w->tn);
  LOG_DEBUG_GLOBAL("            %-10s %-10s", "coarse", "fine");
  LOG_DEBUG_GLOBAL("points:     %-10lu %-10lu", w->N, w->n);
  LOG_DEBUG_GLOBAL("dt:         %-10g %-10g", w->dT, w->dt);
}

workspace_t *workspace_new(MPI_Comm mpicomm, int argc, char **argv)
{
  workspace_t *w = utils_malloc(sizeof(workspace_t));

  /* get MPI stuff */
  w->mpicomm = mpicomm;
  MPI_Comm_size(mpicomm, &(w->mpisize));
  MPI_Comm_rank(mpicomm, &(w->mpirank));

  /* command line options */
  workspace_handle_options(w, argc, argv);
  w->data = fractional_new(w->ic, w->N, w->n, w->dT, w->dt, w->t0,
                           w->alpha, mpicomm);

  return w;
}

void workspace_destroy(workspace_t * w)
{
  fractional_destroy(w->data);

  utils_free(w->filename);
  utils_free(w->ic);
  utils_free(w);
}

void workspace_solve(workspace_t * w)
{
  fractional_parareal(w->data, w->dt, w->dT, 2 * w->mpisize);
}

void workspace_save(workspace_t * w)
{
  MPI_File file;
  MPI_Status status;
  int mpiret;

  size_t n = w->data->fine.n;
  double t = w->t0 + w->mpirank * (n - 1) * w->dt;
  double dt = w->dt;

  size_t data_size = 0;
  double *data = NULL;

  mpiret = MPI_File_open(MPI_COMM_WORLD, w->filename,
                         MPI_MODE_WRONLY | MPI_MODE_CREATE,
                         MPI_INFO_NULL, &file);
  CHECK_MPI(mpiret);

  /* gather the data */
  data_size = 2 * n * sizeof(double);
  data = utils_malloc(2 * n * sizeof(double));
  for (size_t i = 0; i < n; ++i, t += dt) {
    data[2 * i + 0] = w->data->fine.t[i];
    data[2 * i + 1] = w->data->fine.y[i];
  }

  /* write it */
  MPI_File_write_at_all(file, data_size * w->mpirank, data,
                        2 * n, MPI_DOUBLE, &status);

  MPI_File_close(&file);
  utils_free(data);
}
