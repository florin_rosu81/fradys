\documentclass[DIV14,11pt,parskip=half*]{scrartcl}
\usepackage[utf8]{inputenc}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage[ruled,linesnumbered]{algorithm2e}
\DontPrintSemicolon

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing}

% pretty links
\hypersetup{
    colorlinks=true,
    urlcolor=blue,
    citecolor=black,
    linkcolor=black
}

% Custom headers and footers
\pagestyle{fancyplain}
\fancyhead{}
\fancyfoot[L]{}
\fancyfoot[C]{}
\fancyfoot[R]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\setlength{\headheight}{13.6pt}

%-------------------------------------------------------------------------------
%	TITLE SECTION
%-------------------------------------------------------------------------------

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}

\title{
\normalfont \normalsize
\horrule{0.5pt} \\[0.4cm]
\huge \textbf{Parallel Schemes for Fractional Differential Equations} \\
\horrule{2pt} \\[0.5cm]
}
\author{}
\date{}

%-------------------------------------------------------------------------------
%   SHORTCUTS
%-------------------------------------------------------------------------------

\newcommand{\dx}[1]{\,\mathrm{d} #1}
\newcommand{\fd}[1]{\mathrm{D}_*^{\alpha}\, #1}
\newcommand{\ceil}[1]{\left\lceil #1 \right\rceil}
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}

\begin{document}

\maketitle % Print the title

Were trying to solve an ordinary fractional differential equation of the form:
\begin{equation}\label{eq:fde}
\begin{cases}
\fd{y(t)} = f(t, y(t)), & t \in [0, T]\\
y^{(k)}(0) = y_0^{k}, & k \in \{0, \dots, \ceil{\alpha} - 1\},
\end{cases}
\end{equation}
where $\alpha \in [0, 1]$ and $\ceil{\cdot}$ denotes the ceiling function that
rounds up to the nearest integer. The fractional derivative is defined as:
\[
\fd{y(t)} = \frac{1}{\Gamma(\ceil{\alpha} - \alpha)}
            \int_0^T \frac{y^{(\ceil{\alpha})}(t)}{(t - \tau)^{\alpha - \ceil{\alpha}+ 1}} \dx{\tau}.
\]

The numerical method used to solve~\eqref{eq:fde} is a fractional version of the
Adams-Bashforth-Moulton method (a classic predictor-collector method). The
domain $[0, T]$ is discretized into $N$ intervals with a step size $h = \frac{T}{N}$
and the grid points $t_n = nh$, for $n \in \{0, \dots, N\}$. We will also denote
$y_n = y(t_n)$ and $f_n = f(t_n, y_n)$ with $y_0$ as the initial condition.
Since we restrict $\alpha$ to the $[0, 1]$ interval, we do not require any of
the higher derivatives of the initial condition.

The first step of the scheme is the \textbf{predictor}, which will give a
first approximation $y_{n + 1}^P$ of our solution:
\begin{equation}\label{eq:abm}
y_{n + 1}^P = \sum_{k = 0}^{\ceil{\alpha} - 1} \frac{t_{n + 1}^k}{k!} y_0^{(k)} +
              h^\alpha \sum_{k = 0}^n b_{n - k} f_k,
\end{equation}
where the weights $b_{n}$ are given by:
\[
b_n = \frac{(n + 1)^\alpha + n^\alpha}{\Gamma(\alpha + 1)}.
\]

The second and final approximation of our solution, called the \textbf{corrector},
is given by:
\[
y_{n + 1} = \sum_{k = 0}^{\ceil{\alpha} - 1} \frac{t_{n + 1}^k}{k!} y_0^{(k)} +
            h^\alpha \left(c_n f_0 + \sum_{k = 1}^n a_{n - k} f_k +
                           \frac{f(t_{n + 1}, y^P_{n + 1})}{\Gamma(\alpha + 2)}
                     \right),
\]
where the weights $a_n$ and $c_n$ are defined as:
\[
a_n = \frac{(n + 2)^{\alpha + 1} - 2(n + 1)^{\alpha + 1} + n^{\alpha + 1}}{\Gamma(\alpha + 2)}
\]
and
\[
c_n = \frac{n^{\alpha + 1} - (n - \alpha)(\alpha + 1)^\alpha}{\Gamma(\alpha + 2)}.
\]

The most difficult part of numerically solving such equations consists in the
fact that at each step, we require the complete history of the variable. That
is to say, to compute $y_{n + 1}$, we need to know all the previous values
$y_k$ that are used to compute $f_k$, for $k \leq n$. This makes fractional
differential equations notoriously hard to parallelize.

Some attempts have been made, see~\cite{D2011} for a new method created specifically
for fractional differential equations or~\cite{XHC2014} for an adaptation of
the \emph{Parareal}~\cite{LMT2001} method to this kind of equations.

\section{Dynamic Scheme}

The first algorithm we're going to look at proposes to break up all the $N$
time steps between the $P$ processes we have, giving:
\[
N_P = \frac{N}{P}
\]
steps per process (see Figure~\ref{fig:partition})

\begin{figure}[!ht]
\centering
\begin{tikzpicture}[scale=2]
\draw [thick] (0, 0) -- (7, 0);
\foreach[count=\i from 0] \x in {0, 0.5,...,2}{
    \draw[ultra thick] (\x, -0.05) -- (\x, 0.05);
    \node at (\x, -0.05) [below] {$t_{\i}$};
}
\foreach[count=\i from 5]\x in {5, 5.5,...,7}{
    \pgfmathtruncatemacro{\n}{10 - \i};
    \draw[ultra thick] (\x, -0.05) -- (\x, 0.05);
    \node at (\x, -0.05) [below] {$t_{N - \n}$};
}

\draw [decorate,decoration={brace,amplitude=10pt,mirror}] (0,-0.4) -- (2,-0.4);
\draw [decorate,decoration={brace,amplitude=10pt,mirror}] (5,-0.4) -- (7,-0.4);

\node at (1, -0.6) [below] {Process $p_0$};
\node at (6, -0.6) [below] {Process $p_P$};
\node at (3.5, -0.2) {$\dots$};
\end{tikzpicture}
\caption{Partition of time steps between processes.}
\label{fig:partition}
\end{figure}

During an iteration $n$, a process $p$ that represents the block in which
we can find $n$, that is to say if
\[
n \in \left[N_Pp, N_P(p + 1)\right]
\]
will compute the value of $y_n$. This is done by computing a partial sum in
each process $p' < p$ and gathering the results to finally form the complete
sum as defined in~\eqref{eq:abm}.

One major downside of the algorithm we are going to describe is that given a
fix number $P$ of processes, most of them will be idle for a big part of the
computations. To be exact, they will be idle as long as:
\[
\floor{\frac{n}{N_P}} < p,
\]
that is to say, as long as the current iteration has not reached the block
corresponding to process $p$. This can be solved by adding new processes
as the iteration progress or with iterative algorithms like the \emph{parareal}
method described in the next section.

\begin{algorithm}[!ht]
    \caption{Parallel Algorithm for the Adams-Bashforth-Moulton scheme.}
    \label{alg:parallelabm}
    \KwData{$T$ end of the time interval.}
    \KwData{$N$ global number of points.}
    \KwData{$P$ number of processes.}
    \KwData{$p$ current process.}
    $N_P \leftarrow N / P$\;
    $n_{min} \leftarrow N_Pp$\;
    $n_{max} \leftarrow N_P(p + 1)$\;
    $y_0 \leftarrow$ initial condition\;
    \For{$n \in [1, N]$}{
        $S \leftarrow 0$\;
        $p' \leftarrow \floor{\frac{n}{N_P}}$ \tcc*[r]{process computing $y_n$}
        \If{$p > p'$}{
            continue\;
        }
        \eIf{$p = p'$}{
            \tcc{receive partial sums from previous processes}
            \For{$p' \in [0, p - 1]$}{
                \texttt{MPI\_Recv}$(p', S_{p'})$\;
                $S \leftarrow S + S_{p'}$\;
            }
            \tcc{compute local sum}
            \For{$k \in [n_{min}, n - 1]$}{
                $S \leftarrow S + b_{n - k} f_k$\;
            }
            \tcc{compute the predictor at time $t_n$}
            $y_n^P \leftarrow y_0 + S$\;
        }{
            \tcc{compute the partial sum and send it to the right process}
            \For{$k \in [n_{min}, n_{max}]$}{
                $S \leftarrow S + b_{n - k} f_k$\;
            }
            \texttt{MPI\_Send}$(p', S)$\;
        }
        Repeat the same algorithm to compute the corrector $y_{n}$\;
    }
\end{algorithm}

\section{Parareal}

Originally, the \emph{Parareal Method} (see~\cite{LMT2001} and~\cite{MT2005})
was developed to \emph{parallelize in time} a whole class of PDEs of the form:
\begin{equation}\label{eq:pararealpde}
\begin{cases}
\partial_t u(t, \mathbf{x}) + Au(t, \mathbf{x}) = 0, & t \in [T_0, T], \\
u(T_0, \mathbf{x}) = u_0(\mathbf{x}),
\end{cases}
\end{equation}
where $A$ is a functional operator on some space $V$. Assuming the operator
$A$ is differential operator, it can be spatially discretized and incorporated
into a function $f$, effectively transforming our PDE into an ODE.

\subsection{The Parareal Method for Classical ODEs}

We will now be looking at numerically solving ODEs of the following form:
\begin{equation}\label{eq:pararealode}
\begin{cases}
y'(t) = f(t, y(t)), & t \in [0, T], \\
y(T_0) = y_0,
\end{cases}
\end{equation}
where the right hand side function $f$ can be a linear function (e.g. coming
from the spacial discretization of~\eqref{eq:pararealpde}) or a non-linear
function.

The parareal method is an iterative method that allows parallelizing the
numerical resolution of~\eqref{eq:pararealode} (and~\eqref{eq:pararealpde})
in time. This parallelization can be carried out in numerous ways. In this
paper, we will use an MPI library to parallelize the resolution (OpenMP or
other multiprocessing programming paradigms can be used as well).

We suppose that we have $P$ processes in our process pool and divide the initial
interval equally between them:
\[
0 = \tau_0 < \tau_1 \dots < \tau_P = T,
\text{ where } \tau_p = p \Delta \tau \text{ and } \Delta \tau = \frac{T}{P}.
\]
The interval belonging to each process is denoted by $\Omega_p = [\tau_p, \tau_{p + 1}]$,
for $p \in [0, P - 1]$. Independently of the process-dependant grid, we also
define, on each $\Omega_p$, a coarse and fine discretization (see
Figure~\ref{fig:parareal}) such that:
\[
\Delta \tau \geq \Delta T \gg \Delta t.
\]
This definition allows the possibility that the coarse discretization of
$\Omega_p$ is just its two boundaries, in which case we can do away with the
intermediary coarse discretization altogether. Usually, the parareal method
is used with $\Delta \tau = \Delta T$, but we used this generalized version
to account for cases where we have a large $T$ and a small number of processes.

We will denote by:
\begin{itemize}
    \item $\Upsilon_p$ the approximation of $y(\tau_p)$ at the coarsest level,
    for $p \in [0, P - 1]$,
    \item $Y_j$ the approximation obtained on the coarse discretization
    of $\Omega_p$ defined above, for $j \in [0, N - 1]$, and
    \item $y_j$ the approximations obtained on the fine discretization
    of $\Omega_p$, for $j \in [0, n]$.
\end{itemize}

\begin{figure}[!ht]
\centering
\begin{tikzpicture}
\draw (0, 0) -- (10, 0);
\foreach \x in {0, 2, ..., 10}
    \draw [fill] (\x, 0) circle (0.07);

\draw (8, 1.5) -- (10, 1.5);
\foreach \x in {8, 9, ..., 10}
    \draw [fill] (\x, 1.5) circle (0.07);

\draw (8, 3) -- (10, 3);
\foreach \x in {8, 8.5, ..., 10}
    \draw [fill] (\x, 3) circle (0.07);

\node at (1, 0.5) {$\Delta \tau$};
\node at (0, -0.1) [below] {$\tau_0$};
\node at (2, -0.1) [below] {$\tau_1$};
\node at (5, -0.4) {$\dots$};
\node at (8, -0.1) [below] {$\tau_{P - 1}$};
\node at (10, -0.1) [below] {$\tau_P$};
\node at (-0.6, -0.06) [below] {$0 = $};
\node at (10.7, -0.06) [below] {$= T$};

\node at (8.5, 1.5) [above] {$\Delta T$};
\node at (8, 1.5) [below] {$T_0$};
\node at (9, 1.1) {$\dots$};
\node at (10, 1.5) [below] {$T_N$};

\node at (8.25, 3) [above] {$\Delta t$};
\node at (8, 3) [below] {$t_0$};
\node at (9, 2.7) {$\dots$};
\node at (10, 3) [below] {$t_n$};
\end{tikzpicture}
\caption{Example discretization of the $[0, T]$ time interval: first we divide
it into $P$ intervals corresponding to each process $p$, then each process
divides it's subinterval into a coarse grid of $N$ points and a fine grid of $n$
points.}
\label{fig:parareal}
\end{figure}

To compute $Y_j$ and $y_j$, we define:
\begin{itemize}
    \item $\mathcal{F}$, a fine integrator that operates on the fine discretization
    of $\Omega_p$. We also denote by $y_n = \mathcal{F}(y_0, \tau_p,
    \tau_{p + 1})$ the numerical solution obtained by integrating $y$ on the
    interval $\Omega_p$ with an initial condition $y_0$.
    \item $\mathcal{G}$, a coarse integrator which operates on the coarse
    discretization of $\Omega_p$. We also denote by $Y_N = \mathcal{G}(y_0,
    \tau_p, \tau_{p + 1})$ the numerical solution obtained by integrating $y$
    on the interval $\Omega_p$ with an initial condition $y_0$.
\end{itemize}

Generally, the Parareal method is defined as:
\begin{equation}\label{eq:parareal}
\Upsilon_{p + 1}^{k + 1} = \mathcal{G}(\Upsilon_p^{k + 1}, \tau_p, \tau_{p + 1}) +
                           \mathcal{F}(\Upsilon_p^k, \tau_p, \tau_{p + 1}) -
                           \mathcal{G}(\Upsilon_p^k, \tau_p, \tau_{p + 1}),
\end{equation}
for $p \in [0, P - 1]$ and $k$ as the iteration counter for the method. The
initial values for the iteration, $\Upsilon_p^0$ can be computed using the
coarse integrator $\mathcal{G}$ sequentially on each interval $\Omega_p$.

We can see in~\eqref{eq:parareal}, that, while the computations of $\mathcal{G}$
are tied to each other, the fine solution from $\mathcal{F}$ can be computed
in parallel (since $\Upsilon_p^k$ is known from the previous iteration). A full
parallel implementation using MPI is sketched in Algorithm~\ref{alg:parareal}.

\begin{algorithm}[!ht]
    \caption{Parareal Method for solving non-linear ODEs.}
    \label{alg:parareal}
    \KwData{$p$ current process.}
    \KwData{$K$ number of iterations of the Parareal algorithm.}
    $\Upsilon_0^0 \leftarrow $ initial condition\;
    \tcc{sequential computation on coarse grid}
    \For{$p' \in [0, p]$}{
        $\mathbf{Y}_p'^0 \leftarrow \mathcal{G}(\Upsilon_{p'}^0, \tau_{p'}, \tau_{p' + 1})$\;
        $\Upsilon_{p' + 1}^0 \leftarrow Y_N^0$\;
    }
    \tcc{parareal iteration}
    \For{$k \in [0, K - 1]$}{
        \tcc{compute the fine solution on the current process}
        $\mathbf{y}_p^{k + 1} \leftarrow \mathcal{F}(\Upsilon_p^k, \tau_p, \tau_{p + 1})$\;
        \tcc{receive the new solution at $\tau_p$}
        \eIf{$p == 0$}{
            $\Upsilon_p^{k + 1} \leftarrow$ initial condition\;
        }{
            \texttt{MPI\_Recv}($\Upsilon_p^{k + 1}$, p - 1)\;
        }
        \tcc{compute a new coarse solution}
        $\mathbf{Y}_p^{k + 1} \leftarrow \mathcal{G}(\Upsilon_p^{k + 1}, \tau_p, \tau_{p + 1})$\;
        \tcc{correction}
        $\Upsilon_{p + 1}^{k + 1} \leftarrow Y_N^{k + 1} + y_n^{k + 1} - Y_N^k$\;
        \tcc{send the new solution at $\tau_{p + 1}$}
        \If{$p < P - 1$}{
            \texttt{MPI\_Send}($\Upsilon_{p + 1}^{k + 1}$, p + 1)\;
        }
    }
\end{algorithm}

Some extra notation is used in the algorithm, specifically vectors
$\mathbf{Y}_p^k = (Y_0^k, \dots, Y_j^k, \dots Y_N^k)_p$ and
$\mathbf{y}_p^k = (y_0^k, \dots, y_j^k, \dots y_n^k)_p$ that denote the
solution on $\Omega_p$ at each time step $T_j$ and $t_j$ on the coarse and
fine grids, respectively. Even though the algorithm only requires
the values at the end of each interval $\Omega_p$ (i.e. $Y_N^p$ and $y_n^p$), we
save the whole solution because it practice it is usually necessary for
visualization afterwards.

We can see that at the end of Algorithm~\ref{alg:parareal}, each process
will have a fine solution $\mathbf{y}_p^K$ for all the points on the fine
discretization of $\Omega_p$. It can be proven that the vector
$\mathbf{y}$ formed by the concatenation of all the $\mathbf{y}_p^K$ is equal
to the vector obtained if we were to do a single fine integration on the whole
domain, for $K \to \infty$.

More stability and convergence properties of the Parareal method
can be found in~\cite{LMT2001}.

\subsection{The Parareal Method for Fractional ODEs}

The Parareal method has been adapted for use with fractional ODEs in~\cite{XHC2014}.
We will try to expand on it by giving a more detailed explanation as well as an
algorithm for implementing the method using MPI.

The big issue with using the Parareal method with fractional ODEs is that,
unlike the classic case where we only required the approximation at process
boundaries, we will require all the previous values for advancing in time.

For this purpose, we will define the memory for the coarse and fine
grids on each process as:
\begin{itemize}
    \item $\mathbf{Y}_{1:p}$ which is the vector formed from the concatenation
    of all the $\mathbf{Y}_{p'} \in \mathbb{R}^N$.
    \item $\mathbf{y}_{1:p}$ which is the vector formed from the concatenation
    of all the $\mathbf{y}_{p'} \in \mathbb{R}^n$.
\end{itemize}

Using this notation, the Parareal method for fractional ODEs can be written as:
\begin{equation}\label{eq:pararealfde}
\mathbf{Y}_{p + 1}^{k + 1} = \mathcal{G}(\mathbf{Y}_{1:p}^{k + 1}, \tau_p, \tau_{p + 1}) +
                             \mathcal{F}(\mathbf{y}_{1:p}^k, \tau_p, \tau_{p + 1}) -
                             \mathcal{G}(\mathbf{Y}_{1:p}^k, \tau_p, \tau_{p + 1}),
\end{equation}
where the coarse integrator on the interval $\Omega_p$ takes as memory the
coarse solutions on the previous intervals, and the fine integrator takes into
account the fine memory. A slightly different method has been described
in~\cite{XHC2014}, where both the coarse and the fine integrator use the fine
grids as memory.

%% TODO: should ask the guys if that's what they really meant.

In our implementations we have used the Adams-Bashforth-Moulton scheme for
the fine integrator on each $\Omega_p$ and for the coarse integrator, we have
used:
\begin{itemize}
    \item the same Adams-Bashforth-Moulton scheme or
    \item a classic implicit ODE scheme such as Backward Euler or Crank-Nicolson.
    Such a scheme can be used by approximating our original equation~\eqref{eq:fde}
    with an ODE (i.e. supposing $\alpha = 1$).
\end{itemize}

\begin{algorithm}[!ht]
    \caption{Parareal Method for solving fractional ODEs.}
    \label{alg:pararealfde}
    \KwData{$p$ current process.}
    \KwData{$K$ number of iterations of the Parareal algorithm.}
    $Y_0^0 \leftarrow $ initial condition\;
    \tcc{sequential computation on coarse grid}
    \For{$p' \in [0, p]$}{
        $\mathbf{Y}_{p'}^0 \leftarrow \mathcal{G}(\mathbf{Y}_{0:p' - 1}^0, \tau_{p'}, \tau_{p' + 1})$\;
    }
    $\mathbf{y}_{p}^0 \leftarrow$ interpolate from $\mathbf{Y}_{p}$\;
    \;
    \For{$k \in [0, K - 1]$}{
        \tcc{compute the fine solution on the current process}
        \texttt{PARALLEL\_DFFT\_CONVOLUTION}($\mathbf{b}, f(\mathbf{y}^k)$)\;
        $\mathbf{y}_p^{k + 1} \leftarrow \mathcal{F}(\mathbf{y}_{0:p - 1}^k, \tau_{p}, \tau_{p + 1})$\;
        \;
        \tcc{compute a new coarse solution sequentially}
        $\mathbf{Y}_p^{k + 1} \leftarrow$ interpolate from $\mathbf{y}_p^{k + 1}$\;
        $\mathbf{Y}_p^{k + 1} \leftarrow \mathcal{G}(\mathbf{Y}_{0:p - 1}^{k + 1}, \tau_{p}, \tau_{p + 1})$\;
        \;
        \tcc{correction with interpolation}
        $\mathbf{y}_p^{k + 1} \leftarrow \mathbf{Y}_p^{k + 1} + \mathbf{y}_p^{k + 1} - \mathbf{Y}_p^k$\;
    }
\end{algorithm}

As in Algorithm~\ref{alg:parallelabm}, the biggest issue is computing the
convolutions that make up the sum in each time step. Unlike the previous
proposed algorithm, when using the Parareal algorithm we have, at each time
step, all the values of $a_n$, $b_n$, $c_n$ and $f_n$ for all the points in
the coarse and fine grids on each process. This allows us to greatly parallelize
the computation of the convolutions by using a \textbf{Discrete Fast Fourier
Transform} and its inverse.

The discrete convolution theorem can be written for two vectors
$\mathbf{x}, \mathbf{y} \in \mathbb{R}^d$ as:
\[
\mathbf{x} * \mathbf{y} = \mathrm{DFFT}^{-1}[\mathrm{DFFT}[\mathbf{x}] \cdot \mathrm{DFFT}[\mathbf{y}]],
\]
where $\cdot$ in this case is the element-wise multiplication of the resulting
vectors from the transformation. This allows us to do the following
simplification for the predictor step of the Adams-Bashforth-Moulton scheme:
\[
y_{n + 1}^{k + 1} = y_0 + h^\alpha (\mathbf{b} * \mathbf{f}^k)_n,
\]
where $\mathbf{b}$ is the vector of coefficients from the scheme and
$\mathbf{f}^k = (f(t_0, y_0^k), \dots, f(t_{NP}, y_{NP}^k))$ is the vector
containing all the function values obtained by using the values of $y$ from the
previous iteration $k$.

In the case of the convolution, taking the $n$-th element from the resulting
vector would give us exactly the desired sum:
\[
(\mathbf{b} * \mathbf{f}^k)_n = \sum_{j = 0}^n b_{n - j} f(t_j, y_j^k).
\]

\section{Conclusions}

Nothing works! We're all gonna die! Life is pointless!

\begin{thebibliography}{9}
\bibitem{LMT2001}
    J.-L. Lions, Y. Maday, G. Turinici,
    \emph{A ``Parareal'' in Time Discretization of PDEs},
    Comptes Rendus de l'Academie des Sciences Paris, Vol. 332, p. 661-668, 2001.
\bibitem{MT2005}
    Y. Maday, G. Turinici,
    \emph{The Parareal in Time Iterative Solver: A Further Direction to Parallel Implementation},
    Lecture Notes in Computational Science and Engineering, Vol. 40, p. 441-448, 2005.
\bibitem{D2011}
    K. Diethelm,
    \emph{An Efficient Parallel Algorithm for the Numerical Solution of Fractional Differential Equations},
    Fractional Calculus and Applied Analysis, Vol. 14, p. 475-490, 2011.
\bibitem{XHC2014}
    Q. Xu, J. S. Hesthaven, F. Chen,
    \emph{A Parareal Method for Time-Fractional Differential Equations},
    Elsevier, 2014.
\end{thebibliography}

\end{document}
