# - Find the FFTW library
#
# Usage:
#   find_package(FFTW COMPONENTS [COMPONENTS] [REQUIRED] [QUIET])
#
# The components are:
#   CORE                    ... base fftw library
#   THREADS                 ... fftw with POSIX threads
#   OPENMP                  ... fftw with OpenMP threads
#   MPI                     ... fftw with MPI
#
# It sets the following variables:
#   FFTW_FOUND              ... true if fftw is found on the system
#   FFTW_LIBRARIES          ... full path to fftw library
#   FFTW_INCLUDES           ... fftw include directory
#
# The following variables will be checked by the function
#   FFTW_USE_STATIC_LIBS    ... if true, only static libraries are found
#   FFTW_ROOT               ... if set, the libraries are exclusively searched
#                               under this path
#   FFTW_PRECISION          ... can take values SINGLE, DOUBLE, LONGDOUBLE to
#                               specify the precision for fftw
#

# if environment variable FFTWDIR is specified, it has same effect as FFTW_ROOT
if(NOT FFTW_ROOT)
    if(ENV{FFTWDIR})
        set(FFTW_ROOT $ENV{FFTWDIR})
    else()
        set(FFTW_ROOT "/usr")
    endif()
endif()

# check whether to search static or dynamic libs
set(CMAKE_FIND_LIBRARY_SUFFIXES_SAV ${CMAKE_FIND_LIBRARY_SUFFIXES})
if(${FFTW_USE_STATIC_LIBS})
    set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_STATIC_LIBRARY_SUFFIX})
else()
    set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_SHARED_LIBRARY_SUFFIX})
endif()

# macro for declaring components
set(_FFTW_PRIVATE_COMPONENTS)
macro(fftw_declare_component _NAME)
    list(APPEND ${_FFTW_PRIVATE_COMPONENTS} ${_NAME})
    set("_FFTW_PRIVATE_COMPONENTS_${_NAME}" ${ARGN})
endmacro()

if(NOT DEFINED FFTW_PRECISION)
    set(_FFTW_PRECISION_SUFFIX "")
elseif(${FFTW_PRECISION} STREQUAL "SINGLE")
    set(_FFTW_PRECISION_SUFFIX "f")
elseif(${FFTW_PRECISION} STREQUAL "LONGDOUBLE")
    set(_FFTW_PRECISION_SUFFIX "l")
else()
    set(_FFTW_PRECISION_SUFFIX "")
endif()

fftw_declare_component(CORE     fftw3${_FFTW_PRECISION_SUFFIX})
fftw_declare_component(THREADS  fftw3${_FFTW_PRECISION_SUFFIX}_threads)
fftw_declare_component(OPENMP   fftw3${_FFTW_PRECISION_SUFFIX}_omp)
fftw_declare_component(MPI      fftw3${_FFTW_PRECISION_SUFFIX}_mpi)

# always include core
if(NOT FFTW_FIND_COMPONENTS)
    set(FFTW_FIND_COMPONENTS "CORE")
endif()

# check components
foreach(_FFTW_PRIVATE_COMPONENT ${FFTW_FIND_COMPONENTS})
    if(NOT DEFINED _FFTW_PRIVATE_COMPONENTS_${_FFTW_PRIVATE_COMPONENT})
        message(FATAL_ERROR "Unknown FFTW component: ${_FFTW_PRIVATE_COMPONENT}")
    endif()
endforeach()

# find components
set(FFTW_LIBRARIES )
foreach(_FFTW_PRIVATE_COMPONENT ${FFTW_FIND_COMPONENTS})
    find_library(
        FFTW_COMPONENTS_${_FFTW_PRIVATE_COMPONENT}_LIBRARY
        NAMES ${_FFTW_PRIVATE_COMPONENTS_${_FFTW_PRIVATE_COMPONENT}}
        PATHS ${FFTW_ROOT}
        PATH_SUFFIXES "lib" "lib64"
        NO_DEFAULT_PATH)

    if(${FFTW_COMPONENTS_${_FFTW_PRIVATE_COMPONENT}_LIBRARY})
        set(FFTW_${_FFTW_PRIVATE_COMPONENT}_FOUND TRUE)
    else()
        set(FFTW_${_FFTW_PRIVATE_COMPONENT}_FOUND TRUE)
    endif()

    list(APPEND FFTW_LIBRARIES ${FFTW_COMPONENTS_${_FFTW_PRIVATE_COMPONENT}_LIBRARY})
endforeach()

# find include dirs
find_path(
    FFTW_INCLUDES
    NAMES "fftw3.h"
    PATHS ${FFTW_ROOT}
    PATH_SUFFIXES "include"
    NO_DEFAULT_PATH)

set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES_SAV})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(FFTW DEFAULT_MSG
                                  FFTW_INCLUDES FFTW_LIBRARIES)

mark_as_advanced(FFTW_INCLUDES FFTW_LIBRARIES)

