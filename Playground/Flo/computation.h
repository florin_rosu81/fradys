#ifndef COMPUTATION_H
#define COMPUTATION_H

#define MAX_VALUES 3000000 
extern long int maxValue;
extern int send_data_tag; 
extern int return_data_tag;

void compute_master(int procID, int totalProc);

void compute_slave(int procID, int totalProc);
#endif
