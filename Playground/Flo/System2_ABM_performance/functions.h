#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include "defines.h"
extern DOUBLE a;
extern DOUBLE b;

extern DOUBLE q1;
extern DOUBLE q2;

extern DOUBLE beta;
extern DOUBLE gama;

extern DOUBLE f;
extern DOUBLE omega;

extern DOUBLE h;

extern DOUBLE timeSim;

extern DOUBLE B[MAX_VALUES][2];
extern DOUBLE C[MAX_VALUES][2];
extern DOUBLE F[MAX_VALUES][2];


void initFunctions();

DOUBLE g(DOUBLE x);
void funct(DOUBLE t, DOUBLE *x, DOUBLE *y);

void computeB(int index);
void computeC(int index);
#endif
