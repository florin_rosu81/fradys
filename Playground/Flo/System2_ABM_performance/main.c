#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include "computation.h"

int main(int argc, char **argv)
{
    clock_t clockStart, clockEnd;
    time_t timeStart, timeEnd;
    double time_clock;
    int ierr, num_procs, my_id;
    INT i;
    INT valuesCompute;
    INT toPrint = 50000;
    char *c;
    FILE *f;

    int data_tag = 2000;
    //send_data_tag = 2*data_tag;
    //return_data_tag = 2*data_tag + 1;

    ierr = MPI_Init(&argc, &argv);


    /* find out MY process ID, and how many processes were started. */

    ierr = MPI_Comm_rank(MPI_COMM_WORLD, &my_id);
    ierr = MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

    procId = my_id;
    procTotal = num_procs;

    if (my_id == 0)
    {
	clockStart = clock();
	timeStart = time(NULL);
	printf("%s",ctime(&timeStart));

    }
    valuesCompute = compute();


    if (my_id == 0)
    {
	clockEnd = clock();
	timeEnd = time(NULL);
	time_clock = ((double) (clockEnd - clockStart)) / CLOCKS_PER_SEC;



	printf("%20.10f--secconds for %ld elements and %d proccessess with absolute time %f\n", time_clock, valuesCompute, num_procs, difftime(timeEnd, timeStart));
	printf("%s",ctime(&timeEnd));
	
	f = fopen("outputPoints.txt","wt");
	for (i=(valuesCompute>toPrint)?valuesCompute-toPrint:0; i<valuesCompute; i++)
	    fprintf(f, "%20.18f   %20.18f \n",SOL[i][0], SOL[i][1]);
	fclose(f);
    }

    ierr = MPI_Finalize();
    return ierr;
}
        
