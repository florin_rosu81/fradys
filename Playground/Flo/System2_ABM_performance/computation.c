#include "computation.h"
#include "functions.h"
#include <mpi.h>
#include <math.h>
#include <stdio.h>

int procId;

int procTotal;
INT maxValue;
DOUBLE partialValues[4];//Predictor x, predictor y, corector x, corector y
DOUBLE sumValues[4];
DOUBLE SOL[MAX_VALUES][2];

int checkReturn(INT t)
{

    INT threshold = 5000;
    if ((F[t-1][0]>threshold) ||(F[t-1][0]<-threshold) )
	return 1;
    if ((F[t-1][1]>threshold) ||(F[t-1][1]<-threshold) )
	return 1;

    return 0;
}

int comunicateValues(INT t)
{
//    MPI_Bcast( F[t-1], 2 , MPI_LONG_DOUBLE, 0,  MPI_COMM_WORLD);

    
    return checkReturn(t);
}

void computation(INT t)
{
    INT lower, upper, i;

    computeC(t);

    INT delta = t % procTotal;
    INT load = t / procTotal;
    if (procId<delta)
    {
	lower = procId * load + procId; 
	upper = lower + load + 1;
    }
    else
    {
	lower = procId * load + delta;
	upper = lower + load;
    }

    partialValues[0] = partialValues[1] = partialValues[2] = partialValues[3] = 0;

    for (i=lower; i<upper; i++)
    {
	partialValues[0] += B[i][0]*F[t-i-1][0];
	partialValues[1] += B[i][1]*F[t-i-1][1];
	partialValues[2] += C[i][0]*F[t-i-1][0];
	partialValues[3] += C[i][1]*F[t-i-1][1];
    }
    return;
    DOUBLE* bPointer = &(B[lower][0]);
    DOUBLE* cPointer = &(C[lower][0]);
    DOUBLE* fPointer = &(F[t-lower-1][0]);
    for (i=lower; i<upper; ++i, ++bPointer, ++cPointer, ++fPointer)
    {
	partialValues[0] += (*bPointer)* (*fPointer);
	partialValues[2] += (*cPointer)* (*fPointer);
	++bPointer;
	++cPointer;
	++fPointer;
	partialValues[1] += (*bPointer) * (*fPointer);
	partialValues[3] += (*cPointer)* (*fPointer);
    }

}

void comunicateResults(INT t)
{
    MPI_Allreduce(&partialValues, &sumValues, 4, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
}

INT compute()
{
    /*
	if (procId != 0) 
		return;
		*/
	INT i;
	initFunctions();
	maxValue = timeSim / h;
	DOUBLE h_q1 = pow(h,q1);
	DOUBLE h_q2 = pow(h,q2);
	DOUBLE tgammaq11 = h_q1/tgamma(q1+1);
	DOUBLE tgammaq21 = h_q2/tgamma(q2+1);

	DOUBLE tgammaq12 = h_q1/tgamma(q1+2); 
	DOUBLE tgammaq22 = h_q2/tgamma(q2+2);

	DOUBLE v0[2];
	v0[0] = F[0][0];
	v0[1] = F[0][1];
	SOL[0][0] = v0[0];
	SOL[0][1] = v0[1];

	funct(0, &F[0][0], &F[0][1]);
	for (i=1; i<maxValue; i++)
	{
		//comunicate to proccesses the values
		if (comunicateValues(i)!=0)
		{
		    return i;
		}
		

		//compute convolution
		computation(i);

		//gather the result
		comunicateResults(i);

		//compute the function value
		
		    // sumValues[4];//Predictor x, predictor y, corector x, corector y
		    // sq1, sq2, s1, s2



		    DOUBLE p1 = v0[0] + tgammaq11 * sumValues[0];
		    DOUBLE p2 = v0[1] + tgammaq21 * sumValues[1];

		    funct(i*h, &p1, &p2);
		    DOUBLE y1 = v0[0] + tgammaq12 * (p1+sumValues[2]);
		    DOUBLE y2 = v0[1] + tgammaq22 * (p2+sumValues[3]);

		    SOL[i][0] = y1;
		    SOL[i][1] = y2;

		    funct(i*h, &y1, &y2);

		    F[i][0] = y1;
		    F[i][1] = y2;
	}
	return i;
}
