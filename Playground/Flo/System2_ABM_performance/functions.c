#include "functions.h"
#include <stdio.h>
#include <math.h>
#include <string.h>
DOUBLE a;
DOUBLE b;
DOUBLE q1;
DOUBLE q2;
DOUBLE beta;
DOUBLE gama;
DOUBLE f;
DOUBLE omega;
DOUBLE h;
DOUBLE timeSim;


DOUBLE B[MAX_VALUES][2];
DOUBLE C[MAX_VALUES][2];
DOUBLE F[MAX_VALUES][2];

#define PI 3.14159265
#define N 20

void processParameter(char *line)
{
    char* stringValue = strchr(line,'=');

    if (stringValue == NULL)
        return;

    stringValue++;

    *(stringValue-1) = '\0';

    DOUBLE paramValue;
    sscanf(stringValue, "%f",&paramValue);

   
    //printf("PROCESS %s with value %Lf\n",line, paramValue);
    if (strcmp(line,"h")==0)
    {
        h = paramValue;
    }
    if (strcmp(line,"q1")==0)
    {
        q1 = paramValue;
    }
    if (strcmp(line,"q2")==0)
    {
        q2 = paramValue;
    }
    if (strcmp(line,"time")==0)
    {
	timeSim = paramValue;
    }
    if (strcmp(line,"gama")==0)
    {
	gama = paramValue;
    }
    if (strcmp(line,"omega")==0)
    {
	omega = paramValue;
    }
    if (strcmp(line,"f")==0)
    {
	f = paramValue;
    }
    if (strcmp(line,"Fx")==0)
    {
	F[0][0] = paramValue;
    }
    if (strcmp(line,"Fy")==0)
    {
	F[0][1] = paramValue;
    }
    if (strcmp(line,"a")==0)
    {
	a = paramValue;
    }
    if (strcmp(line,"b")==0)
    {
	b = paramValue;
    }
    if (strcmp(line,"beta")==0)
    {
	beta = paramValue;
    }

}
void getParameters()
{
    char line[N];
    FILE *f = fopen("inputParameters.txt","rt");

    if (f==NULL)
    {
        printf("ERROR - default parameters");
        return;
    }

    while (!feof(f))
    {
        fgets(line, N, f);
        processParameter(line);
    }
}
void initFunctions()
{
	
        getParameters();
}

DOUBLE g(DOUBLE x)
{
	if (x>=1)
		return b*x + a - b;
	if (x<=-1)
		return b*x -a + b;
	return a*x;
}
void funct(DOUBLE t, DOUBLE *x, DOUBLE *y)
{
    DOUBLE newX = *y - g(*x);

    //newY = -beta*F[index-1][1] -beta*gama*F[index-1][1]  -beta*F[index-1][0] + f*sin(omega*(index-1)*h * PI / 180);
    DOUBLE newY = -beta * (*y + gama*(*y) + (*x)) + f*sin(omega*t);

    *x = newX;
    *y = newY;
}

DOUBLE computeComb(DOUBLE index, DOUBLE q)
{
    //printf("%d %Lf: %LF %LF",index, q, lgammal(q+1), lgammal(index+1));
    return tgamma(q + 1 ) / (tgamma(index + 1) * tgamma(-(q - index)+1)) ;
}
void computeC(int index)
{
    //C[index-1]

    int k = index - 1;
    if (index >=2)
    {
	C[index-2][0] = pow(k+1,q1+1) - 2*pow(k, q1+1) + pow(k-1, q1+1);
	C[index-2][1] = pow(k+1,q2+1) - 2*pow(k, q2+1) + pow(k-1, q2+1);
    }
    C[index-1][0] = pow(index-1, q1+1) - (index-1-q1)*pow(index,q1);
    C[index-1][1] = pow(index-1, q2+1) - (index-1-q2)*pow(index,q2);

    B[index-1][0] = pow(index, q1) - pow(index-1, q1);
    B[index-1][1] = pow(index, q2) - pow(index-1, q2);
}
void computeB(int index)
{
}
