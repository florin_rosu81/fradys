#include "computation.h"
#include "functions.h"
#include <mpi.h>
#include <math.h>
#include <stdio.h>

#include <omp.h>
int procId;

int procTotal;
_INT maxValue;
_DOUBLE partialValues[2];//Predictor[0]  corector[0]
_DOUBLE sumValues[2];
_DOUBLE SOL[MAX_VALUES][2];

int checkReturn(_INT t)
{
    return 0; _DOUBLE originaSolution = _POW(t*h,2*alfa) - t*h*t*h;   _DOUBLE delta = 5000000;

    if ((SOL[t][1] > originaSolution+delta) || (SOL[t][1] < originaSolution - delta))
	return 1;
    return 0;
}

void computation(_INT t)
{
    _INT lower, upper, i;

    computeB(t);
    computeC(t);

    _INT delta = t % procTotal;
    _INT load = t / procTotal;
    if (procId<delta)
    {
	lower = procId * load + procId; 
	upper = lower + load + 1;
    }
    else
    {
	lower = procId * load + delta;
	upper = lower + load;
    }

    partialValues[0] = partialValues[1];

    _DOUBLE a0, a1;
    a0 = a1 = 0;
#pragma omp parallel for default(shared) reduction(+:a0) reduction(+:a1)
    for (i=lower; i<upper; i++)
    {
	a0 += B[i]*F[t-i-1];
	a1 += C[i]*F[t-i-1];
    }
    partialValues[0] = a0;
    partialValues[1] = a1;
 

}

void comunicateResults(_INT t)
{
    MPI_Allreduce(&partialValues, &sumValues, 2, _MPI_DATA_TYPE, MPI_SUM, MPI_COMM_WORLD);
}

_INT compute()
{
	_INT i;
	_DOUBLE upperInterval = 10;
	initFunctions();
	maxValue = upperInterval / h;

	_INT toPrintStatus = maxValue / 100;
	_DOUBLE v0;
	v0 = F[0];
	SOL[0][0] = 0;
	SOL[0][1] = v0;

	funct(0, &F[0]);
	for (i=1; (i<maxValue)&&(i<MAX_VALUES); i++)
	{
		//compute convolution
		computation(i);

		//gather the result
		comunicateResults(i);

		_DOUBLE p1 = v0 + _POW(h, alfa)/_GAMMA(alfa+1) * sumValues[0];
		_DOUBLE y1;
		if (ABM_Usage == 1.0)
		{

		    funct(i*h, &p1);
		    y1 = v0 + _POW(h,alfa)/_GAMMA(alfa+2) * (p1+sumValues[1]);

		}
		else
		{
		    // p1 ii y_{n+1}^p lui
		    _DOUBLE yp = p1;

		    funct(i*h, &p1);
		    //p1 ii de fapt f_1(t_{n+1}, y_{n+1}^p)
		    _DOUBLE zp = _POW(h,alfa)/_GAMMA(alfa+2) * p1;

		    _DOUBLE xc = yp+zp;
		    funct(i*h, &xc);
		    y1 = yp + _POW(h,alfa)/_GAMMA(alfa+2) * xc; //+sumValues[1]);
		}
		SOL[i][0] = i*h;
		SOL[i][1] = y1;
		
		if (i%toPrintStatus == 0)
		    if (procId == 0)
			printf("%ld %%\n",i/toPrintStatus);

		if (checkReturn(i) != 0)
		    return i;

		funct(i*h, &y1);

		F[i] = y1;
	}
	return i;
}
