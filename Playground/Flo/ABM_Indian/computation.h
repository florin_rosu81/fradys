#ifndef COMPUTATION_H
#define COMPUTATION_H
#include "defines.h"
extern int procId;
extern int procTotal;
extern _INT maxValue;

extern _DOUBLE SOL[MAX_VALUES][2];
_INT compute();
#endif
