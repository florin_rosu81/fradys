#/bin/bash

VALUES="0.1 0.2 0.4 0.6 0.75 0.9"
CURRDIR=`pwd`
	for i in $VALUES; do
		TEST_DIR=${CURRDIR}/${i}
        	mkdir $TEST_DIR
		cd $TEST_DIR

		TEST_NAME=s2system${i}_

		TEST_EXE=${TEST_DIR}/${TEST_NAME}
		TEST_CMD=${TEST_DIR}/${TEST_NAME}.cmd
		TEST_PAR=${TEST_DIR}/inputParameters.txt



		cp ${CURRDIR}/run_sim.cmd ${TEST_CMD}
		cp ${CURRDIR}/run_sim ${TEST_EXE}
		cp ${CURRDIR}/inputParameters.txt ${TEST_PAR}

		echo mpirun -cwd $TEST_DIR -mode SMP -np 256 -exe ${TEST_EXE} >> ${TEST_CMD}
		echo alfa=${i} >> ${TEST_PAR}
		llsubmit ${TEST_CMD}
	done

