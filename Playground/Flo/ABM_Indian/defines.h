#ifndef DEFINES_H
#define DEFINES_H

#ifdef _HIGH_PRECISION

typedef long double _DOUBLE;
#define _POW(A,B) (powl((A),(B)))
#define _GAMMA(A) (tgammal(A))
#define _INPUTFORMAT "%Lf"
#define _OUTPUTFORMAT "%20.18Lf"
#define _MPI_DATA_TYPE MPI_LONG_DOUBLE


#else //NOT HIGH PRECISION
typedef double _DOUBLE;
#define _POW(A,B) (pow((A),(B)))
#define _GAMMA(A) (tgamma(A))
#define _INPUTFORMAT "%lf"
#define _OUTPUTFORMAT "%20.18lf"
#define _MPI_DATA_TYPE MPI_DOUBLE



#endif//HIGH_PRECISION



typedef long int _INT;

#define MAX_VALUES 1000004

#endif
