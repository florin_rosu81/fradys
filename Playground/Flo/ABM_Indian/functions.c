#include "functions.h"
#include <stdio.h>
#include <math.h>
#include <string.h>
_DOUBLE alfa;
_DOUBLE h;
_DOUBLE ABM_Usage;//can have 1 for abm, 0 for indian stuff

_DOUBLE B[MAX_VALUES];
_DOUBLE C[MAX_VALUES];
_DOUBLE F[MAX_VALUES];

#define PI 3.14159265
#define N 20

void processParameter(char *line)
{
    char* stringValue = strchr(line,'=');

    if (stringValue == NULL)
        return;

    stringValue++;

    *(stringValue-1) = '\0';

    _DOUBLE paramValue;
    sscanf(stringValue, _INPUTFORMAT ,&paramValue);

    /*
    printf("\n%s = ",stringValue);
    printf(_OUTPUTFORMAT,paramValue);
    */
   
    if (strcmp(line,"h")==0)
    {
        h = paramValue;
    }
    if (strcmp(line,"alfa")==0)
    {
        alfa = paramValue;
    }
    if (strcmp(line,"Fy")==0)
    {
	F[0] = paramValue;
    }
    if (strcmp(line,"ABM_Usage")==0)
    {
	ABM_Usage = paramValue;
	if (ABM_Usage==1.0)
	    printf("\nWe are using ABM\n");
	else
	    printf("\nWe are using Indian method\n");
    }
}
void getParameters()
{
    char line[N];
    FILE *f = fopen("inputParameters.txt","rt");

    if (f==NULL)
    {
        printf("ERROR - default parameters");
        return;
    }

    while (!feof(f))
    {
        fgets(line, N, f);
        processParameter(line);
    }
}
void initFunctions()
{
    getParameters();
}

void funct(_DOUBLE t, _DOUBLE *y)
{
    
    _DOUBLE newY = _GAMMA(2*alfa + 1) * _POW(t, alfa) /_GAMMA(alfa+1) - 2*_POW(t,2-alfa)/_GAMMA(3-alfa) + _POW(_POW(t,2*alfa) - (t)*(t), 4)-_POW(*y,4);
    *y = newY;
}

void computeC(_INT index)
{
    _INT k = index - 1;
    if (index >=2)
    {
	C[index-2] = _POW(k+1,alfa+1) - 2*_POW(k, alfa+1) + _POW(k-1, alfa+1);
    }
    C[index-1] = _POW(index-1, alfa+1) - (index-1-alfa)*_POW(index,alfa);

}
void computeB(_INT index)
{
    B[index-1] = _POW(index, alfa) - _POW(index-1, alfa);
}
