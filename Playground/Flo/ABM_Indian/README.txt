COD
- codul ii scris sa mearga si cu double si cu long double. Totul e dat doar de optiunea de
compilare -D _HIGH_PRECISION daca e prezenta in Makefile ca si CFLAG. daca o stergi va fi cu
double si o sa mearga super repede ;)

- in computation.c in function compute() acolo este hardcodata valoarea maxima pentru interval
(upperInterval) cu valoarea 10... ca de la 0 la 10 deja sunt probleme, nu tre' sa mergem pana la
200. valoarea asta daca vrei poti sa o parametrizezi.

- checkReturn ii functia care face totul sa se opreasca daca valorile deviaza cu 0.5 fata de
functia originala

- fisierul  inputParameters.txt contine parametrii, valorile lui alfa, valorile initiale si pasul
h. Poti sa adaugi upperInterval, si modifici putin codul in function.c 

- defines.h e cel mai important ;) si cel mai banal ;) acolo is definite tipurile de date si ce functii de gamma si
pow sa foloseasca, precum si formatul de la citiri si afisari

- in main se initializeaza MPI, se pornesc calculele ;) si la final afiseaza in fisier punctele
rezultate.

PC
- compilare
trebuie sa ai instalat MPI si openMP. Pe compurile de la faculta in lab-uri sunt deja instalate.

make

- rulare
editezi inputParameters.txt cu ce valoare de alfa si pasul h vrei sa rulezi
mpirun -np 8 ./run_sim

- pe ecran iti arata progresul si la final ai fisierul outputPoints_10.txt care sa il plotezi 


BLUEGENE
- compilare
make -f Makefile_BG

- lansare teste
pe wiki-ul de la bluegene iti zice cum se compileaza/lanseaza un job. trebuie sa ai un fisier cmd care sa
contina o gramada de informatii...blablabla. Fiecare job trebuie sa aiba nume unic, nu poti lansa
2 executabile cu acelasi nume. DAR...

Scriptul launchTests.sh face tot pentru tine ;) Il editezi cu ce valori de alfa-uri vrei sa lansezi job-uri. Ideea e ca
fiecare job sa aiba o cale diferite si/sau nume diferit. In functie de valorile de alfa se creaza
un director si acolo vor fi rezultatele.

./launchTests.sh

(pentru alfa=0.1)
va crea folderul 0.1 si acolo o sa se lanseze job-ul (cu toate chestiile care le cere bluegene-ul)
In fisierul .out o sa poti sa vezi "progresul" in procente (dai din cand in cand "tail blabla.out") si la final va crea un fisier
outputPoints_10.txt (de la 0.10), fisierul care il iei de pe bluegene si il plotezi la tine pe
comp (cu sftp transferi fisiere de la tine de pe comp pe bluegene, si invers... wiki )

pentru valoarea lui alfa de 0.75 o sa fie folderul 0.75 si outputPoints_75.txt





