#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include "defines.h"

extern _DOUBLE alfa;

extern _DOUBLE h;

extern _DOUBLE B[MAX_VALUES];
extern _DOUBLE C[MAX_VALUES];
extern _DOUBLE F[MAX_VALUES];


extern _DOUBLE ABM_Usage;
void initFunctions();

void funct(_DOUBLE t, _DOUBLE *y);

void computeB(_INT index);
void computeC(_INT index);
#endif
