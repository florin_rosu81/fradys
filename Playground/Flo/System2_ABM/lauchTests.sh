#/bin/bash

#VALUES="1 2 3 4 5 7 8 10"
#TEST_IDX="8 16 32 64 128 256"
VALUES="10 15 20 25 30"
TEST_IDX="1024"
CURRDIR=`pwd`

for j in $TEST_IDX; do
	for i in $VALUES; do
		TEST_DIR=${CURRDIR}/${i}_${j}
        	mkdir $TEST_DIR
		cd $TEST_DIR

		TEST_NAME=s2system${i}${j}_

		TEST_EXE=${TEST_DIR}/${TEST_NAME}
		TEST_CMD=${TEST_DIR}/${TEST_NAME}.cmd
		TEST_PAR=${TEST_DIR}/inputParameters.txt



		cp ${CURRDIR}/run_sim.cmd ${TEST_CMD}
		cp ${CURRDIR}/run_sim ${TEST_EXE}
		cp ${CURRDIR}/inputParameters.txt ${TEST_PAR}

		TIME=${i}0000

		echo mpirun -cwd $TEST_DIR -mode VN -np ${j} -exe ${TEST_EXE} >> ${TEST_CMD}
		echo time=${TIME} >> ${TEST_PAR}
		llsubmit ${TEST_CMD}
	done
done


