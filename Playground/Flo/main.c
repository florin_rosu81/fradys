#include <time.h>
#include <stdio.h>
#include "mpi.h"
#include "functions.h"
#include "computation.h"

main(int argc, char **argv)
{
    clock_t clockStart, clockEnd;
    time_t timeStart, timeEnd;
    double time_clock;
    int ierr, num_procs, my_id;
    int i;
    char *c;
    maxValue = MAX_VALUES;
    if (argc >= 2)
    {
	maxValue = 0;
	for (c = argv[1]; *c != '\0'; c++)
	{
	    maxValue *= 10;
	    maxValue += *c - '0';
	}
    }
    if (maxValue > MAX_VALUES)
	maxValue = MAX_VALUES;

    int data_tag = 2000;
    if (argc >= 3)
    {
	data_tag = 0;
	for (c = argv[2]; *c != '\0'; c++)
	{
	    data_tag *= 10;
	    data_tag += *c - '0';
	}
    }
    send_data_tag = 2*data_tag;
    return_data_tag = 2*data_tag + 1;

    h = 0.01;
    q = 0.8;
    ierr = MPI_Init(&argc, &argv);


    /* find out MY process ID, and how many processes were started. */

    ierr = MPI_Comm_rank(MPI_COMM_WORLD, &my_id);
    ierr = MPI_Comm_size(MPI_COMM_WORLD, &num_procs);


    if (my_id == 0)
    {
	clockStart = clock();
	timeStart = time(NULL);
	printf(ctime(&timeStart));

	compute_master(my_id, num_procs);

	clockEnd = clock();
	timeEnd = time(NULL);
	time_clock = ((double) (clockEnd - clockStart)) / CLOCKS_PER_SEC;

    }
    else
    {
	compute_slave(my_id, num_procs);
    }



    if (my_id == 0)
    {
	printf("%20.10f--secconds for %ld elements and %d proccessess with absolute time %f\n", time_clock, maxValue, num_procs, difftime(timeEnd, timeStart));
	printf(ctime(&timeEnd));
    }

    ierr = MPI_Finalize();
}
        
