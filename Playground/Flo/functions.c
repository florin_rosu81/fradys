#include <math.h>
#include "functions.h"

long double h = 0.1;
long double q = 0.8;



void function(long double*x, long double* y, long double* z)
{
    long double a =  1.;
    long double b = 3.;
    long double c = 1.;
    long double d = 5.;
    long double p = (b - d)/a;
    long double eps = 0.005;
    long double s = 4.;
    long double I = 3.25;
    long double x1 = -1.618033988749895;
    long double xx = *y - a * pow(*x,3) + b * pow(*x,2) + I - *z;
    long double yy = c - d * pow(*x,2) - *y;
    long double zz = eps * (s * (*x - x1) - *z);

    *x=xx;
    *y=yy;
    *z=zz;
}

long double getB(int k)
{
    /*return k+1;*/
    int j=k+1;
    return pow(j,q) - pow(j-1,q);

}
long double getC(int k) 
{
    return  pow(k,q + 1) - (k-q)*pow(k+1,q);
}
