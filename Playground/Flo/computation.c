#include <stdio.h>
#include <mpi.h>
#include <math.h>

#include "computation.h"
#include "functions.h"

int send_data_tag; 
int return_data_tag;
FILE *file;

long double FSOL[MAX_VALUES][3];
long double B[MAX_VALUES];
long double C[MAX_VALUES];


long double SOL[MAX_VALUES][3];

long double convolution_value[6];
long int current;
long int maxValue;

void updateC(int index)
{

	int k = index-1;
	
	if (index >= 2)
	{
		C[index-1-1] = pow(k + 1,q + 1) - 2*(pow(k,q + 1)) + pow(k - 1,q + 1);
	}
	
	C[index-1] = getC(k);
}	
void printSolution(int index)
{
    int i;
    if (index % 10000)
	return;
    for (i=0; i<3; i++)
    {
	fprintf(file, "%20.18Lf ", FSOL[index][i]);
	/*
	if (i!=2)
	    fprintf(file,", ");
	    */
    }

    printf("%d\n",index);
    fprintf(file, "\n");

}

void printInfosVector(char *message, long double *info, int size)
{
	printf("%s:", message);
	int i;
	for (i=0; i<size; i++)
		printf("%20.18Lf ",info[i]);
	printf("\n");	
}

void printInfosMatrix(char *message, long double info[][3], int size, int col)
{
	printf("%s:", message);
	int i,j;
	for (i=0; i<size; i++)
	{
		for (j=0; j<col; j++)
			printf("%20.18Lf ",info[i][j]);
		printf("\n");
	}
	printf("\n");	
}

void compute_master( int procID, int totalProc)
{
    long int i,j;
    long double dataToSend[3], partialSum[6];
    long double gamma1 = 0.9313837709802428;
    long double gamma2 = 1.6764907877644364;
    MPI_Status status;
    int error, an_id;
    current = 0;

    SOL[0][0] = 0.1;
    SOL[0][1] = 0.1;
    SOL[0][2] = 0.1;

    //MPI_Barrier(MPI_COMM_WORLD);
    file = fopen("Values.txt","wt");

    for (i=0; i<maxValue-1; i++)
    {
       	//printf("computation....%ld\n",current);
	FSOL[current][0] = SOL[current][0];
	FSOL[current][1] = SOL[current][1];
	FSOL[current][2] = SOL[current][2];
	
	function(FSOL[current], FSOL[current]+1, FSOL[current]+2);
	printSolution(current);
	if (i == maxValue - 1)
	    break;
/*
	for(an_id = 1; an_id < totalProc; an_id++)
	{ 
	    error = MPI_Send( FSOL[current], 3 , MPI_LONG_DOUBLE,
	                  an_id, send_data_tag, MPI_COMM_WORLD);
	}
*/
//	for (j=0; j<6; j++)
//	    convolution_value[j] = 0;
	//MPI_Barrier(MPI_COMM_WORLD);
	error = MPI_Bcast( FSOL[current], 3 , MPI_LONG_DOUBLE, 0,  MPI_COMM_WORLD);
	//convolution 0-3 = B * FSOL  ---- S1
	//convolution 4-6 = C * FSOL  ---- S2
	
	compute_slave(procID, totalProc);
/*
	for(an_id = 1; an_id < totalProc; an_id++)
	{ 
	    error = MPI_Recv( &partialSum, 6 , MPI_LONG_DOUBLE,
	                  an_id, return_data_tag, MPI_COMM_WORLD, &status);
	    for(j=0; j<6; j++)
		convolution_value[j]+= partialSum[j];
	}
*/
//	printInfosVector("B ", B, current);
//	printInfosVector("C ", C, current);

//	printInfosVector("S1 ", convolution_value, 3);
//	printInfosVector("S2 ", convolution_value+3, 3);


	//convolution 0-3 = B * FSOL  ---- S1
	//convolution 4-6 = C * FSOL  ---- S2
	

	//S11 = S1 *  ((h**q) / mt.gamma(q+1)) // fiecare element din S1 se inmulteste cu asta
	long double S11[3];
	for (j=0; j<3; j++)
	    S11[j] = convolution_value[j] * pow(h,q)/gamma1;
	
//	printInfosVector("S11 ", S11, 3);
	//vp = v0 + s11 // astia is vectori toti
	long double vp[3];
	for (j=0; j<3; j++)
	    vp[j] = SOL[0][j] + S11[j];
	
	
//	printInfosVector("P ",vp, 3);
	//compute f(vp) for future use;
	function(vp, vp+1, vp+2);

//	printInfosVector("F(P) ",vp, 3);
	
	//newSol = v0 + [f(vp) + S2]  * ((h**q) / mt.gamma(q+2))) // tot produs scalar
	
	//fs2 = f(vp) + s2
	long double fs2[3];
	for (j=0; j<3; j++)
		fs2[j] = vp[j] + convolution_value[3+j];

//	printInfosVector("fs2 ",fs2,3);
	long double s22[3];
	for (j=0; j<3; j++)
		s22[j] = fs2[j] * pow(h, q) / gamma2;

//	printInfosVector("s22 ",s22, 3);
	for (j=0; j<3; j++)
	    SOL[current][j] = SOL[0][j] + s22[j] ;

//	printInfosMatrix("SOL:",SOL,current+1, 3);
    }

    fclose(file);
}

void compute_slave(int procID, int totalProc)
{
    long int indexStart, indexEnd;
    long int i, j;
    int error;
    long double dataToReceive[3];
    long double partialSum[6];
    MPI_Status status;
    if (procID != 0)
    {
	current = 0;
	//MPI_Barrier(MPI_COMM_WORLD);
    }


    do
    {
	B[current] = getB(current);
	updateC(current+1); //C[current] = getC(current);

	//printf("the current value is %ld\n",current);
	//printInfosVector("B ", B, current+1);
	//printInfosVector("C ", C, current+1);
	if (procID != 0)
	{
	    //MPI_Barrier(MPI_COMM_WORLD);
	    error = MPI_Bcast( &dataToReceive, 3 , MPI_LONG_DOUBLE, 0,  MPI_COMM_WORLD);
	    /*
	    error = MPI_Recv( &dataToReceive, 3, MPI_LONG_DOUBLE, 
		0, send_data_tag, MPI_COMM_WORLD, &status);
*/

	    for (i=0; i<3; i++)
		FSOL[current][i] = dataToReceive[i];
	}

	current++;
	indexStart = procID * (current / totalProc);

	if (procID == (totalProc - 1))
	    indexEnd = current;
	else
	    indexEnd = (procID + 1) * (current / totalProc);
	
	for (j=0; j<6; j++)
	    partialSum[j] = 0;
	/*printf("\nPROC %d compute for current %ld:",procID, current);*/
	long double *bPointer, *cPointer;
	bPointer = B+indexStart;
	cPointer = C+indexStart;
	for (i=indexStart; i < indexEnd; i++, bPointer++, cPointer++)
	{
	    for (j=0; j<3; j++)
		partialSum[j] += *bPointer * FSOL[current - i - 1][j];
	    for (j=0; j<3; j++)
		partialSum[3+j] += *cPointer * FSOL[current - i - 1][j];
		
	    /*printf("%d PROC = %ld-%ld=(%4.2lf,%4.2lf)      \n  ",procID, i, current - i - 1, B[i], FSOL[current - i - 1][0]);*/
	}

	if (procID == 0)
	    for(j=0; j<6; j++)
		convolution_value[j]+= partialSum[j];
/*
	else
	    error = MPI_Send( partialSum, 6 , MPI_LONG_DOUBLE,
	                  0, return_data_tag, MPI_COMM_WORLD);
*/
	MPI_Reduce(&partialSum, &convolution_value, 6, MPI_LONG_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

//	    MPI_Barrier(MPI_COMM_WORLD);

/*	
	printf("PROC %d compute:",procID);
	for (i=0; i<current; i++)
	{
	    printf("(%4.2f,%4.2f) ",coef[i], func[i]);
	}
	printf("\n");
	*/

    }while ((procID != 0)&&(current < maxValue-1));
}
