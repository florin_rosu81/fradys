
#/bin/bash

VALUES="10"
TEST_IDX="8 16 24 32"
CURRDIR=`pwd`

for j in $TEST_IDX; do
	for i in $VALUES; do
		TEST_DIR=${CURRDIR}/${i}_${j}
		mkdir $TEST_DIR
		cd $TEST_DIR

		TEST_NAME=convolution${i}${j}

		TEST_EXE=${TEST_DIR}/${TEST_NAME}
		TEST_CMD=${TEST_DIR}/${TEST_NAME}.cmd


		cp ${CURRDIR}/convolution.cmd ${TEST_CMD}
		cp ${CURRDIR}/convolution ${TEST_EXE}

		NR_VALUES=${i}00000

		echo mpirun -cwd $TEST_DIR -mode VN -np ${j} -exe ${TEST_EXE} -args \"${NR_VALUES}\" >> ${TEST_CMD} 
		llsubmit ${TEST_CMD}
	done
done
