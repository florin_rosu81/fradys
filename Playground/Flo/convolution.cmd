#!/bin/sh
 
# @ job_name = convolution 
# @ job_type = bluegene
# @ requirements = (Machine == "$(host)")
# @ error = $(job_name)_$(jobid).err
# @ output = $(job_name)_$(jobid).out
# @ environment = COPY_ALL;

# @ notification = never 
# @ notify_user = florin.rosu81@e-uvt.ro

# @ class = small 
# @ bg_size = 64
# @ queue

mpirun -cwd /GPFS/users/invites/frosu/Convolution/OUT/ -mode VN -np 4 -exe /GPFS/users/invites/frosu/Convolution/convolution -args "894"
