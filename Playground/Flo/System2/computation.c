#include "computation.h"
#include "functions.h"
#include <mpi.h>
#include <math.h>
#include <stdio.h>

int procId;

int procTotal;
INT maxValue;
DOUBLE partialValues[2];
DOUBLE sumValues[2];

int checkReturn(INT t)
{

    INT threshold = 5000;
    if ((F[t-1][0]>threshold) ||(F[t-1][0]<-threshold) )
	return 1;
    if ((F[t-1][1]>threshold) ||(F[t-1][1]<-threshold) )
	return 1;

    return 0;
}

int comunicateValues(INT t)
{
    MPI_Bcast( F[t-1], 2 , MPI_LONG_DOUBLE, 0,  MPI_COMM_WORLD);

    
    return checkReturn(t);
}

void computation(INT t)
{
    INT lower, upper, i;

    computeC(t-1);

    lower = 0;
    upper = t;
    lower = procId * (t / procTotal);

    if (procId == (procTotal - 1))
	upper = t;
    else
	upper = (procId + 1) * (t / procTotal);


    partialValues[0] = partialValues[1] = 0;

    for (i=lower; i<upper; i++)
    {
	partialValues[0] += C[i][0]*F[t-i-1][0];
	partialValues[1] += C[i][1]*F[t-i-1][1];
    }

}

void comunicateResults(INT t)
{
    MPI_Reduce(&partialValues, &sumValues, 2, MPI_LONG_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
}

INT compute()
{
    /*
	if (procId != 0) 
		return;
		*/
	INT i;
        long int toPrint;
	long int toPrintValues = 68000;
	FILE *f;
	initFunctions();
	maxValue = timeSim / h;

	if (procId == 0)
	    f = fopen ("outputPoints.txt","wt");

        if (maxValue<5000)
            toPrint = 1;
        else
            toPrint = maxValue / toPrintValues;


	for (i=1; i<maxValue; i++)
	{
		//comunicate to proccesses the values
		if (comunicateValues(i)!=0)
		{
		    if (procId == 0)
			fclose(f);
		    return i;
		}
		

		//compute convolution
		computation(i);

		//gather the result
		comunicateResults(i);

		//compute the function value
		
		if (procId == 0)
		{
		    F[i][0] = sumValues[0];
		    F[i][1] = sumValues[1];

		    computeElement(i);

		    if ( /*(i % toPrint == 0) ||*/ ((maxValue - i)< toPrintValues) )
			fprintf(f, "%20.18Lf   %20.18Lf \n",F[i][0] ,F[i][1]); 
		}
	}
	if (procId == 0)
	    fclose(f);

	return i;
}
