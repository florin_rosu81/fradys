#!/bin/bash

#TEST_VALUES="0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1"
TEST_VALUES="0.1"

CURRDIR=`pwd`
for h in $TEST_VALUES; do
    for q1 in $TEST_VALUES; do
        for q2 in $TEST_VALUES; do
            TEST_NAME="h_${h}___q1_${q1}___q2_${q2}"
            TEST_DIR=${CURRDIR}/${TEST_NAME}

            mkdir $TEST_DIR
            cd $TEST_DIR

            cp ${CURRDIR}/run_sim ${TEST_DIR}/${TEST_NAME}

            echo "h="${h} >> ${TEST_DIR}/inputParameters.txt
            echo "q1="${q1} >> ${TEST_DIR}/inputParameters.txt
            echo "q2="${q2} >> ${TEST_DIR}/inputParameters.txt

            mpirun -np 1 ${TEST_NAME}
        done
    done
done
