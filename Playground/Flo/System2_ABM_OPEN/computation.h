#ifndef COMPUTATION_H
#define COMPUTATION_H
#include "defines.h"
extern int procId;
extern int procTotal;
extern INT maxValue;

extern DOUBLE SOL[MAX_VALUES][2];
INT compute();
#endif
