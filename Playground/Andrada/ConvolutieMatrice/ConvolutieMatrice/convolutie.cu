﻿#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <helper_functions.h>
#include <helper_cuda.h>
#include <helper_timer.h>
#include <helper_math.h>

#define KERNEL_SIZE 5
#define BLOCK_SIZE 16
#define MATRIX_SIZE 100

typedef struct {
	unsigned int latime;
	unsigned int inaltime;
	float* allElem;
} Matrice;

// matrice latime * inaltime
Matrice AlocaMatriceD(int latime, int inaltime)
{
	Matrice a;
	cudaMalloc((void **)&a.allElem, sizeof(float)*latime*inaltime);
	a.latime = latime;
	a.inaltime = inaltime;
	return a;
}
// Alocă matrice pe host de dimensiune inaltime*latime
Matrice AlocaMatrice(int latime, int inaltime)
{
	Matrice A;
	A.latime = latime;
	A.inaltime = inaltime;
	int dim = A.latime * A.inaltime;
	A.allElem = (float*)malloc(dim*sizeof(float));
	return A;
}
// Eliberează o matrice de pe host
void dezalocaMatrice(Matrice* A)
{
	free(A->allElem);
	A->allElem = NULL;
}
// Eliberează o matrice de pe device
void dezalocaMatriceD(Matrice* A)
{
	cudaFree(A->allElem);
	A->allElem = NULL;
}
// inmultirea fara memorie partajata
__global__ void MatrixSquare(Matrice A, Matrice B)
{
	int i, j;
	int row_number, column_number;
	float sumaPartiala = 0;
	int width = A.latime;
	row_number = blockIdx.y * blockDim.y + threadIdx.y;
	column_number = blockIdx.x * blockDim.x + threadIdx.x;
	/*
	int threadId = row_number * grid_dimension + column_number
	int depth_number = blockIdx.z * blockDim.z + threadIdx.z;
	*/
	if ((column_number >= 0 && column_number < width) && (row_number >= 0 && row_number < width)) {
		for (int k = 0; k < width; k++){
			sumaPartiala += A.allElem[row_number* width + k] * A.allElem[k * width + column_number];
		}
	}
	if ((row_number < B.inaltime) && (column_number < B.latime)) {
		B.allElem[row_number * B.latime + column_number] = sumaPartiala;
	}
}
/* Matricea B
b0 b1 b2
b3 b4 b5
b6 b7 b8

Matricea f() 
f0 f1 f2
f3 f4 f5
f6 f7 f8

Convolutie k=3: b0*f3+b1*f2+b2*f1+b3*f0
*/
__global__ void KernelConvolutie(Matrice A, Matrice B,float* Vector)
{
	int rowPosition, colPosition;
	
	rowPosition = blockIdx.x * blockDim.x + threadIdx.x;
	colPosition = blockIdx.y * blockDim.y + threadIdx.y;
	if ((rowPosition < MATRIX_SIZE) && (colPosition < MATRIX_SIZE)) {
		/* 1 time step product multiplication */
		//Vector[rowPosition * MATRIX_SIZE + colPosition] = A.allElem[rowPosition*MATRIX_SIZE + colPosition] * B.allElem[(MATRIX_SIZE - 1 - rowPosition) * MATRIX_SIZE + (MATRIX_SIZE - 1 - colPosition)];

		
		/* Compute offset for finding out the location where each thread should write its product multiplications */
		/* Thread (0, 0) computes b0 * f0, b0 * f1, ..., b0 * f8 => it has to save 9 products (offset is 0) */
		/* Thread (0, 1) computes b1 * f0, b1 * f1, ..., b1 * f7 => it has to save 8 products (offset is 0 + 9) */
		/* ... */
		/* Thread (2, 2) computes b8 * f0 => it has to save 1 product (offset is 0 + 9 + 8 + 7 + 6 + ... + 2) */
		int offset = 0;
		for (int i = MATRIX_SIZE * MATRIX_SIZE; i > (MATRIX_SIZE - rowPosition - 1) * MATRIX_SIZE + (MATRIX_SIZE - colPosition); i--) {
			offset += i;
		}

		/* N time steps product multiplication */
		for (int i = 0; i <= (MATRIX_SIZE - 1 - rowPosition) * MATRIX_SIZE + (MATRIX_SIZE - 1 - colPosition); i++) {
			/* If I am thread (0, 0), I will write 9 elements starting with offset 0 => (Vector[0], Vector[1], ... Vector[8]) b0*(f0-f8)*/
			/* If I am thread (0, 1), I will write 8 elements starting with offset 9 (0 + 9) => (Vector[9], Vector[10], ... Vector[16]) b1*(f0-f7)*/
			/* If I am thread (0, 2), I will write 7 elements starting with offset 17 (9 + 8) => (Vector[17], Vector[18], ... Vector[23]) */
			/* Vector will have N * (N - 1) / 2 product multiplications, where N = MATRIX_SIZE * MATRIX_SIZE */
			Vector[i + offset] = A.allElem[rowPosition * MATRIX_SIZE + colPosition] * B.allElem[i];
			//printf("%f ", Vector[i+offset]);
		}
	}
	//printf("\n%d = row %d = col", rowPosition, colPosition);
}

float SumaConvolutie(float* Vector){
	float suma;
	suma = 0;
	for (long i = 0; i < MATRIX_SIZE*MATRIX_SIZE * (MATRIX_SIZE * MATRIX_SIZE + 1) / 2 ; i++)
	{
		suma += Vector[i];
	//printf("Vector[%d] = %f\n", i, Vector[i]);
	
	}
	return suma;
}
void ConvolutieD(const Matrice A, Matrice B)
{
	Matrice Ad, Bd;
	float* Vectorh = (float*)malloc(((MATRIX_SIZE*MATRIX_SIZE) * (MATRIX_SIZE * MATRIX_SIZE + 1) / 2)*sizeof(float));
	memset(Vectorh, 0, ((MATRIX_SIZE*MATRIX_SIZE) * (MATRIX_SIZE * MATRIX_SIZE + 1) / 2)*sizeof(float));
	float* Vectord;
	StopWatchInterface *kernelTime = NULL;
	sdkCreateTimer(&kernelTime);
	sdkResetTimer(&kernelTime);
	FILE *fisierOut = fopen("fisierOut.txt", "a");
	Ad = AlocaMatriceD(A.latime, A.inaltime);
	Bd = AlocaMatriceD(B.latime, B.inaltime);
	checkCudaErrors(cudaMalloc(&Vectord, ((MATRIX_SIZE*MATRIX_SIZE) * (MATRIX_SIZE * MATRIX_SIZE + 1) / 2) * sizeof(float)));
	checkCudaErrors(cudaMemcpy(Ad.allElem, A.allElem, A.latime * A.inaltime * sizeof(float), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(Vectord, Vectorh, ((MATRIX_SIZE*MATRIX_SIZE) * (MATRIX_SIZE * MATRIX_SIZE + 1) / 2) * sizeof(float), cudaMemcpyHostToDevice));
	//dim3 DimBlock(KERNEL_SIZE, KERNEL_SIZE);
	//dim3 DimGrid((A.latime / KERNEL_SIZE) + 1, (A.inaltime / KERNEL_SIZE) + 1);
	dim3 DimBlockKernel(32,32,1);
	dim3 DimGridKernel(MATRIX_SIZE / 32 + 1, MATRIX_SIZE / 32 + 1,1);
	sdkStartTimer(&kernelTime);
	//MatrixSquare << < DimGrid, DimBlock >> > (Ad, Bd);
	KernelConvolutie << < DimGridKernel, DimBlockKernel >> >(Ad, Ad, Vectord);
	checkCudaErrors(cudaMemcpy(Vectorh, Vectord, ((MATRIX_SIZE*MATRIX_SIZE) * (MATRIX_SIZE * MATRIX_SIZE + 1) / 2) * sizeof(float), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaPeekAtLastError());
	checkCudaErrors(cudaDeviceSynchronize());
	/*for (int i = 0; i < MATRIX_SIZE*MATRIX_SIZE; i++)
		printf("\nV[%d]=%f", i, Vectorh[i]); */
	printf("\nSuma este: %f\n", SumaConvolutie(Vectorh));
	cudaThreadSynchronize();
	sdkStopTimer(&kernelTime);
	printf("\nTimp executie GPU: %f ms = %f s \n", sdkGetTimerValue(&kernelTime), sdkGetTimerValue(&kernelTime) / 1000);
	fprintf(fisierOut, "Timp executie kernel: %f ms = %f s \n", sdkGetTimerValue(&kernelTime), sdkGetTimerValue(&kernelTime) / 1000);
	checkCudaErrors(cudaMemcpy(B.allElem, Bd.allElem, Bd.latime * Bd.inaltime * sizeof(float), cudaMemcpyDeviceToHost));
	dezalocaMatriceD(&Ad);
	dezalocaMatriceD(&Bd);
	checkCudaErrors(cudaFree(Vectord));
	free(Vectorh);
}
void genereazaMatrice(Matrice a)
{
	/* int i;
	int size = a.latime * a.inaltime;
	srand(time(NULL));
	for (i = 0; i < size; i++)
	{
		a.allElem[i] = rand()%10;
		if (i % (a.inaltime) == 0)
			printf("\n");
		printf("%.0f ", a.allElem[i]);
	} */
	//memset(a.allElem, 2, a.latime*a.inaltime);
	for (long i = 0; i <  MATRIX_SIZE*MATRIX_SIZE; i++)
	{
		a.allElem[i] = i;
	/*	if (i % (a.inaltime) == 0)
			printf("\n");
		printf("%.0f ", a.allElem[i]);
		*/
	}

}
void afiseazaMatrice(Matrice a){
	for (int i = 0; i < a.latime; i++) {
		for (int j = 0; j < a.inaltime; j++) {
			if ((j % (a.latime)) == 0)
				;
				//printf("\n");
			//printf(" %.0f", a.allElem[i*a.latime + j]);
		}
	}
}
int main(int argc, char** argv)
{
	int latime = 0, inaltime = 0;
	FILE *f;
	if (argc < 2)
	{
		printf("Trimiteți id-ul testului care trebuie rulat\n");
		getchar();
		return 0;
	}
	char testID[50];
	sprintf(testID, "./teste/%s.txt", argv[1]);
	f = fopen(testID, "r");
	fscanf(f, "%d%d", &latime, &inaltime);
	Matrice A;
	Matrice B;//rezultat fără shared memory pe GPU
	A = AlocaMatrice(latime, inaltime);
	B = AlocaMatrice(latime, inaltime);
	printf("Matricea A este: \n");
	genereazaMatrice(A);
	ConvolutieD(A, B);
	cudaDeviceSynchronize();
	printf(cudaGetErrorString(cudaGetLastError()));
	//afiseazaMatrice(B);
	//printf("\nMatricea rezultanta A*A calculata pe GPU\n");
	StopWatchInterface *kernelTime = NULL;
	sdkCreateTimer(&kernelTime);
	sdkResetTimer(&kernelTime);
	sdkStartTimer(&kernelTime);
	dezalocaMatrice(&A);
	dezalocaMatrice(&B);
	fclose(f);
	getchar();
	return 0;
}

