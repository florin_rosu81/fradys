#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>
#include <time.h>
#include <iostream>

#define ALPHA_VALUE 0.9
#define INITIAL_CONDITION_VALUE 0.1
#define STEP_SIZE_VALUE 0.01
#define T_MAX_VALUE 100.00
#define NUMBER_OF_TIME_STEPS ((int)(T_MAX_VALUE / STEP_SIZE_VALUE))
#define BIDIMENSIONAL_VALUE 2

#define FC_VALUE 0.198
#define BE_VALUE 1
#define GA_VALUE 0.015
#define A_VALUE -1.02
#define B_VALUE -0.58
#define OM_VALUE 0.55
#define T_VALUE 0

//#define A_VALUE 1
//#define B_VALUE 3
//#define C_VALUE 1
//#define D_VALUE 5
//#define S_VALUE 4
//#define M_VALUE -1.61803
//#define IP_VALUE 3.25
//#define EPS_VALUE 0.005

#define cudaCheckErrors(cudaError) { \
  if (cudaError != cudaSuccess) { \
    printf("CUDA Failure %s:%d : %s\n", __FILE__, __LINE__, cudaGetErrorString(cudaError)); \
    exit(1); \
  } \
}

#define checkCudaErrors(val) check( (val), #val, __FILE__, __LINE__)

template<typename T>
void check(T err, const char* const func, const char* const file, const int line) {
  if (err != cudaSuccess) {
    std::cerr << "CUDA error at: " << file << ":" << line << std::endl;
    std::cerr << cudaGetErrorString(err) << " " << func << std::endl;
    exit(1);
  }
}

__global__ void convolutionKernel(double *inputValues3D, double *outputValues3D, int currentTimeStep);
__global__ void intermediatePredictorKernel(const double predictorGammaTerm, double *initialConditions3D, double *predictor3D);
__global__ void intermediateCorrectorKernel(const double correctorGammaTerm, double *predictor3D, double *convolutedPredictor3D, double *initialConditions3D, double *corrector3D, double *functionSolution3D, int currentTimeStep);
__device__ void convolutionFunction3D(double *inputValues3D, double *outputValues3D, int currentTimeStep, int columnID);
__device__ void constantVectorMultiplication3D(const double inputConstantValue, double *outputValues3D, int columnID);
__device__ double gFunction(double inputValue);
__device__ void vectorAddition3D(double *firstInputOutputValues3D, double *secondInputValues3D, int columnID);
__global__ void vectorMatrixMultiplication3D(double *vectorInputValue1, double *vectorInputValue2, double *matrixInputValue, double *outputSumVector3D1, double *outputSumVector3D2, int currentTimeStep);

__global__ void coefficientsKernel(double *predictorInputVectorB, double *correctorInputVectorC, double *correctorInputVectorA);

__global__ void sumReduceKernel(double *predictor3DPartialProducts, double *corrector3DPartialProducts, double *predictor3D, double *corrector3D, size_t dynamicSize, int collocatedOffset);

int main(int argc, char **argv) {

  FILE *fisierOut = fopen("fisierOut.txt", "a");

  clock_t cpuStart = clock();
  cudaEvent_t cudaStart, cudaStop;
  cudaEventCreate(&cudaStart);
  cudaEventCreate(&cudaStop);

  /* Start measurement */
  cudaEventRecord(cudaStart, 0);

  static double PREDICTOR_GAMMA_TERM = pow(STEP_SIZE_VALUE, ALPHA_VALUE) / tgamma(ALPHA_VALUE + 1);
  static double CORRECTOR_GAMMA_TERM = pow(STEP_SIZE_VALUE, ALPHA_VALUE) / tgamma(ALPHA_VALUE + 2);
  
  /* Begin Print PREDICTOR_GAMMA_TERM (OK) */
  /*** PREDICTOR_GAMMA_TERM = 0.016479 ***/
  /*
  printf("PREDICTOR_GAMMA_TERM = %.6f\n", PREDICTOR_GAMMA_TERM);
  */
  /* End Print PREDICTOR_GAMMA_TERM (OK) */
  
  /* Begin Print CORRECTOR_GAMMA_TERM (OK) */
  /*** CORRECTOR_GAMMA_TERM = 0.00867315 ***/
  /*
  printf("CORRECTOR_GAMMA_TERM = %.8f\n", CORRECTOR_GAMMA_TERM);
  */
  /* End Print CORRECTOR_GAMMA_TERM (OK) */

  static double *initialConditions3D = (double *) malloc(BIDIMENSIONAL_VALUE * sizeof(double));
  static double *functionSolution3D = (double *) malloc((NUMBER_OF_TIME_STEPS + 1) * BIDIMENSIONAL_VALUE * sizeof(double));

  int *powerOfTwos = (int *) malloc(20 * sizeof(int));
  for (int i = 0; i < 20; i++) {
    powerOfTwos[i] = pow(2, i);
  }

  /*
  cudaStream_t cudaStreams[2];
  cudaStreamCreate(&cudaStreams[0]);
  cudaStreamCreate(&cudaStreams[1]);
  */

  /* CUDA Memory Allocation */
  static double *d_initialConditions3D;
  static double *d_functionSolution3D;
  checkCudaErrors(cudaMalloc((void **) &d_initialConditions3D, BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &d_functionSolution3D, (NUMBER_OF_TIME_STEPS + 1) * BIDIMENSIONAL_VALUE * sizeof(double)));

  static double *d_predictorInputVectorB;
  static double *d_correctorInputVectorC;
  static double *d_correctorInputVectorA;
  static double *d_correctorInputVectorAConcatenatedWithC;
  checkCudaErrors(cudaMalloc((void **) &d_predictorInputVectorB, NUMBER_OF_TIME_STEPS * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &d_correctorInputVectorC, NUMBER_OF_TIME_STEPS * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &d_correctorInputVectorA, NUMBER_OF_TIME_STEPS * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &d_correctorInputVectorAConcatenatedWithC, NUMBER_OF_TIME_STEPS * sizeof(double)));

  static double *d_predictor3DPartialProducts;
  static double *d_corrector3DPartialProducts;
  checkCudaErrors(cudaMalloc((void **) &d_predictor3DPartialProducts, NUMBER_OF_TIME_STEPS * BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &d_corrector3DPartialProducts, NUMBER_OF_TIME_STEPS * BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMemset((void *) d_predictor3DPartialProducts, 0, NUMBER_OF_TIME_STEPS * BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMemset((void *) d_corrector3DPartialProducts, 0, NUMBER_OF_TIME_STEPS * BIDIMENSIONAL_VALUE * sizeof(double)));

  static double *d_predictor3DIntermediateReduction1;
  static double *d_corrector3DIntermediateReduction1;
  checkCudaErrors(cudaMalloc((void **) &d_predictor3DIntermediateReduction1, NUMBER_OF_TIME_STEPS * BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &d_corrector3DIntermediateReduction1, NUMBER_OF_TIME_STEPS * BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMemset((void *) d_predictor3DIntermediateReduction1, 0, NUMBER_OF_TIME_STEPS * BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMemset((void *) d_corrector3DIntermediateReduction1, 0, NUMBER_OF_TIME_STEPS * BIDIMENSIONAL_VALUE * sizeof(double)));
  
  static double *d_predictor3DIntermediateReduction2;
  static double *d_corrector3DIntermediateReduction2;
  checkCudaErrors(cudaMalloc((void **) &d_predictor3DIntermediateReduction2, NUMBER_OF_TIME_STEPS * BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &d_corrector3DIntermediateReduction2, NUMBER_OF_TIME_STEPS * BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMemset((void *) d_predictor3DIntermediateReduction2, 0, NUMBER_OF_TIME_STEPS * BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMemset((void *) d_corrector3DIntermediateReduction2, 0, NUMBER_OF_TIME_STEPS * BIDIMENSIONAL_VALUE * sizeof(double)));

  static double *d_predictor3D;
  static double *d_corrector3D;
  checkCudaErrors(cudaMalloc((void **) &d_predictor3D, BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &d_corrector3D, BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMemset((void *) d_predictor3D, 0, BIDIMENSIONAL_VALUE * sizeof(double)));
  checkCudaErrors(cudaMemset((void *) d_corrector3D, 0, BIDIMENSIONAL_VALUE * sizeof(double)));

  static double *d_convolutedPredictor3D;
  checkCudaErrors(cudaMalloc((void **) &d_convolutedPredictor3D, BIDIMENSIONAL_VALUE * sizeof(double)));

  dim3 dimBlockConvolutionKernel(BIDIMENSIONAL_VALUE, 1, 1);
  dim3 dimGridConvolutionKernel(1, 1);

  for (int i = 0; i < BIDIMENSIONAL_VALUE; i++) {
    *(initialConditions3D + i) = INITIAL_CONDITION_VALUE;
  }

  checkCudaErrors(cudaMemcpy(d_initialConditions3D, initialConditions3D, BIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyHostToDevice));

  /* Apply Convolution Function on the initial conditions */
  convolutionKernel<<<dimGridConvolutionKernel, dimBlockConvolutionKernel>>>(d_initialConditions3D, d_functionSolution3D, 0);
  cudaDeviceSynchronize();
  checkCudaErrors(cudaGetLastError());

  /* Begin Print Convolution Function Applied On Initial Conditions (OK) */
  /*** convolutionFunction(0.1, 0.1, 0.1) = (3.279, 0.85, 0.0338606) ***/
  /*
  checkCudaErrors(cudaMemcpy(functionSolution3D, d_functionSolution3D, NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost));
  printf("functionSolution3D(x%d, y%d, z%d) = [%.7f, %.7f, %.7f]\n", 0, 0, 0,
  functionSolution3D[0 * TRIDIMENSIONAL_VALUE + 0],
  functionSolution3D[0 * TRIDIMENSIONAL_VALUE + 1],
  functionSolution3D[0 * TRIDIMENSIONAL_VALUE + 2]);
  */
  /* End Print Convolution Function Applied On Initial Conditions (OK) */

  dim3 dimBlockKernel(BIDIMENSIONAL_VALUE, 256, 1);
  dim3 dimGridKernel(1, (NUMBER_OF_TIME_STEPS + 255) / 256);

  /* Dimensions used for <<<coefficientsKernel>>> */
  dim3 dimBlockCoefficientsKernel(1024, 1, 1);
  dim3 dimGridCoefficientsKernel((NUMBER_OF_TIME_STEPS + 1023) / 1024, 1);

  /* Compute coefficients */
  coefficientsKernel<<<dimGridCoefficientsKernel, dimBlockCoefficientsKernel>>>(d_predictorInputVectorB, d_correctorInputVectorC, d_correctorInputVectorA);
  cudaDeviceSynchronize();
  checkCudaErrors(cudaGetLastError());
  /* Begin Print Input Vector B Generation (OK) */
  /*** predictorInputVectorB(..., b3, b2, b1, b0) = (..., 0.794327, 0.821809, 0.866066, 1.000000) ***/
  /*
  double *predictorInputVectorB = (double *) malloc(NUMBER_OF_TIME_STEPS * sizeof(double));
  cudaMemcpy(predictorInputVectorB, d_predictorInputVectorB, NUMBER_OF_TIME_STEPS * sizeof(double), cudaMemcpyDeviceToHost);
  for (int i = 0; i < NUMBER_OF_TIME_STEPS; i++) {
    printf("predictorInputVectorB[%d] = %.6f\n", i, predictorInputVectorB[i]);
  }
  */
  /* End Print Input Vector B Generation (OK) */

  /* Begin Print Corrector Input Vector C Generation (OK) */
  /*** correctorInputVectorC(..., c3, c2, c1, c0) = (..., 4.32168, 1.772065 ~ 1.77206, 0.821812, 1.603691 ~ 1.60369) ***/
  /*
  double *correctorInputVectorC = (double *) malloc(NUMBER_OF_TIME_STEPS * sizeof(double));
  /*
  cudaMemcpy(correctorInputVectorC, d_correctorInputVectorC, NUMBER_OF_TIME_STEPS * sizeof(double), cudaMemcpyDeviceToHost);
  for (int i = 0; i < NUMBER_OF_TIME_STEPS; i++) {
    printf("correctorInputVectorC[%d] = %.6f\n", i, correctorInputVectorC[i]);
  }
  */
  /* End Print Corrector Input Vector C Generation (OK) */

  /* Begin Print Corrector Input Vector A Generation (OK) */
  /*** predictorInputVectorA(..., UNUSED VALUE, a2, a1, a0) = (..., 1.48951, 1.53369, 1.59936, 1.73213) ***/
  double *correctorInputVectorA = (double *) malloc(NUMBER_OF_TIME_STEPS * sizeof(double));
  /*
  cudaMemcpy(correctorInputVectorA, d_correctorInputVectorA, NUMBER_OF_TIME_STEPS * sizeof(double), cudaMemcpyDeviceToHost);
  for (int i = 0; i < NUMBER_OF_TIME_STEPS; i++) {
    printf("correctorInputVectorA[%d] = %.5f\n", i, correctorInputVectorA[i]);
  }
  */
  /* End Print Corrector Input Vector A Generation (OK) */

  for (int i = 1; i <= NUMBER_OF_TIME_STEPS; i++) {
    int powerOfTwosIndex = 0;
    for (int j = 0; j < 20; j++) {
      if (powerOfTwos[j] >= i) {
        powerOfTwosIndex = j;
        break;
      }
    }

    dim3 blockSizeReduction(256, BIDIMENSIONAL_VALUE, 1);
    dim3 gridSizeReduction((powerOfTwos[powerOfTwosIndex] + 255) / 256, 1);
    dim3 blockSizeIntermediateReduction(256, BIDIMENSIONAL_VALUE, 1);
    dim3 gridSizeIntermediateReduction((((powerOfTwos[powerOfTwosIndex] + 255) / 256) + 255) / 256, 1);
    dim3 blockSizeFinalReduction((((powerOfTwos[powerOfTwosIndex] + 255) / 256) + 255) / 256, BIDIMENSIONAL_VALUE, 1);
    dim3 gridSizeFinalReduction(1, 1);

    /* Prepare S1 and S2 computation */
    checkCudaErrors(cudaMemcpy(d_correctorInputVectorAConcatenatedWithC, d_correctorInputVectorA, NUMBER_OF_TIME_STEPS * sizeof(double), cudaMemcpyDeviceToDevice));
    checkCudaErrors(cudaMemcpy(d_correctorInputVectorAConcatenatedWithC + i - 1, d_correctorInputVectorC + i - 1, sizeof(double), cudaMemcpyDeviceToDevice));
    /*
    cudaMemcpy(correctorInputVectorA, d_correctorInputVectorA, NUMBER_OF_TIME_STEPS * sizeof(double), cudaMemcpyDeviceToHost);
    for (int j = 0; j < NUMBER_OF_TIME_STEPS; j++) {
      printf("correctorInputVectorA[%d] = %.5f\n", j, correctorInputVectorA[j]);
    }
    */
    /* Compute S1 = Dot(bn, functionSolution3D) */
    /* Compute S2 = Dot(cn :: an, functionSolution3D) */
    vectorMatrixMultiplication3D<<<dimGridKernel, dimBlockKernel>>>(d_predictorInputVectorB, d_correctorInputVectorAConcatenatedWithC, d_functionSolution3D, d_predictor3DPartialProducts, d_corrector3DPartialProducts, i);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());
    /* Begin Print Predictor Partial Products Generation (OK) */
    /*
    double *predictor3DPartialProducts = (double *) malloc(NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE * sizeof(double));
    cudaMemcpy(predictor3DPartialProducts, d_predictor3DPartialProducts, NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost);
    for (int j = 0; j < NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE; j++) {
      printf("predictor3DPartialProducts[%d] = %.7f\n", j, predictor3DPartialProducts[j]);
    }
    */
    /* End Print Predictor Partial Products Generation (OK) */

    /* Begin Print Corrector Partial Products Generation (OK) */
    /*
    double *corrector3DPartialProducts = (double *) malloc(NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE * sizeof(double));
    cudaMemcpy(corrector3DPartialProducts, d_corrector3DPartialProducts, NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost);
    for (int j = 0; j < NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE; j++) {
      printf("corrector3DPartialProducts[%d] = %.7f\n", j, corrector3DPartialProducts[j]);
    }
    */
    /* End Print Corrector Partial Products Generation (OK) */

    sumReduceKernel<<<gridSizeReduction, blockSizeReduction, 2 * 256 * BIDIMENSIONAL_VALUE * sizeof(double)>>>(d_predictor3DPartialProducts, d_corrector3DPartialProducts, d_predictor3DIntermediateReduction1, d_corrector3DIntermediateReduction1, 256 * BIDIMENSIONAL_VALUE, 0);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());

    /* Begin Print Intermediate Predictor 1 Sum Generation (OK) */
    /*
    if (i >= 256) {
    double *predictor3DPartialProducts = (double *) malloc(NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE * sizeof(double));
    cudaMemcpy(predictor3DPartialProducts, d_predictor3DPartialProducts, NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost);
    double *predictor3DIntermediateReduction1 = (double *) malloc(NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE * sizeof(double));
    cudaMemcpy(predictor3DIntermediateReduction1, d_predictor3DIntermediateReduction1, NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost);
    for (int j = 0; j < NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE; j++) {
      printf("predictor3DIntermediateReduction1[%d] = %.6f\n", j, predictor3DIntermediateReduction1[j]);
      printf("predictor3DPartialProducts[%d] = %.7f\n", j, predictor3DPartialProducts[j]);
    }
    }
    */
    /* End Print Intermediate Predictor 1 Sum Generation (OK) */

    /* Begin Print Intermediate Corrector Sum Generation (OK) */
    /*
    double *corrector3DIntermediateReduction = (double *) malloc((powerOfTwos[19] + 255) / 256 * TRIDIMENSIONAL_VALUE * sizeof(double));
    cudaMemcpy(corrector3DIntermediateReduction, d_corrector3DIntermediateReduction, (powerOfTwos[19] + 255) / 256 * TRIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost);
    for (int j = 0; j < TRIDIMENSIONAL_VALUE; j++) {
      printf("corrector3DIntermediateReduction[%d] = %.6f\n", j, corrector3DIntermediateReduction[j]);
    }
    */
    /* End Print Intermediate Corrector Sum Generation (OK) */
    
    sumReduceKernel<<<gridSizeIntermediateReduction, blockSizeIntermediateReduction, 2 * 256 * BIDIMENSIONAL_VALUE * sizeof(double)>>>
      (d_predictor3DIntermediateReduction1, d_corrector3DIntermediateReduction1, d_predictor3DIntermediateReduction2, d_corrector3DIntermediateReduction2, 256 * BIDIMENSIONAL_VALUE, 0);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());
    
    /* Begin Print Intermediate Predictor 2 Sum Generation (OK) */
    /*
    double *predictor3DIntermediateReduction2 = (double *) malloc(NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE * sizeof(double));
    cudaMemcpy(predictor3DIntermediateReduction2, d_predictor3DIntermediateReduction2, NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost);
    for (int j = 0; j < NUMBER_OF_TIME_STEPS * TRIDIMENSIONAL_VALUE; j++) {
      printf("predictor3DIntermediateReduction2[%d] = %.6f\n", j, predictor3DIntermediateReduction2[j]);
    }
    */
    /* End Print Intermediate Predictor 2 Sum Generation (OK) */

    sumReduceKernel<<<gridSizeFinalReduction, blockSizeFinalReduction, 2 * ((((powerOfTwos[powerOfTwosIndex] + 255) / 256) + 255) / 256) * BIDIMENSIONAL_VALUE * sizeof(double)>>>(
      d_predictor3DIntermediateReduction2, d_corrector3DIntermediateReduction2, d_predictor3D, d_corrector3D, ((((powerOfTwos[powerOfTwosIndex] + 255) / 256) + 255) / 256) * BIDIMENSIONAL_VALUE, 1);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());

    /* Begin Print Predictor Sum Generation (OK) */
    double *predictor3DSum = (double *) malloc(BIDIMENSIONAL_VALUE * sizeof(double));
    cudaMemcpy(predictor3DSum, d_predictor3D, BIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost);
    for (int j = 0; j < BIDIMENSIONAL_VALUE; j++) {
      //printf("predictor3DSum / S1[%d] = %.6f\n", j, predictor3DSum[j]);
    }
    /* End Print Predictor Sum Generation (OK) */

    /* Begin Print Corrector Sum Generation (OK) */
    double *corrector3D = (double *) malloc(BIDIMENSIONAL_VALUE * sizeof(double));
    cudaMemcpy(corrector3D, d_corrector3D, BIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost);
    for (int j = 0; j < BIDIMENSIONAL_VALUE; j++) {
      //printf("corrector3D[%d] / S2 = %.6f\n", j, corrector3D[j]);
    }
    /* End Print Corrector Sum Generation (OK) */

    /* Compute Predictor = initialConditions3D + PREDICTOR_GAMMA_TERM * S1 */
    intermediatePredictorKernel<<<dimGridConvolutionKernel, dimBlockConvolutionKernel>>>(PREDICTOR_GAMMA_TERM, d_initialConditions3D, d_predictor3D);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());

    /* Begin Print Predictor Computation (OK) */
    /*** predictor3D0(x, y, z) = (0.154035, 0.114007, 0.100558) ***/
    /*
    double *predictor3D = (double *) malloc(TRIDIMENSIONAL_VALUE * sizeof(double));
    cudaMemcpy(predictor3D, d_predictor3D, TRIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost);
    for (int j = 0; j < TRIDIMENSIONAL_VALUE; j++) {
      printf("predictor3D[%d] = %.6f\n", j, predictor3D[j]);
    }
    */
    /* End Print Predictor Computation (OK) */

    /* Compute Corrector = initialConditions3D + CORRECTOR_GAMMA_TERM * (convolutionFunction3D(predictor, outputValues3D) + S2) */
    /* Compute convolutionFunction3D(Corrector, outputValues3D) */
    intermediateCorrectorKernel<<<dimGridConvolutionKernel, dimBlockConvolutionKernel>>>(CORRECTOR_GAMMA_TERM, d_predictor3D, d_convolutedPredictor3D, d_initialConditions3D, d_corrector3D, d_functionSolution3D, i);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());

    /* Begin Print Corrector Computation (OK) */
    /*** corrector3D0(x, y, z) = (0.174498, 0.118478, 0.100774) ***/
    /*
    double *corrector3D = (double *) malloc(TRIDIMENSIONAL_VALUE * sizeof(double));
    cudaMemcpy(corrector3D, d_corrector3D, TRIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost);
    for (int j = 0; j < TRIDIMENSIONAL_VALUE; j++) {
      printf("corrector3D[%d] = %.6f\n", j, corrector3D[j]);
    }
    */
    /* End Print Corrector Computation (OK) */
    
    /* Begin Print Corrector Convolution Computation (OK) */
    /*** functionSolution3D0(x, y, z) = (3.279000, 0.850000, 0.0336606) ***/
    /*** functionSolution3D1(x, y, z) = (3.3537392 ~ 3.35374, 0.7292743 ~ 0.729274, 0.0353506 ~ 0.0353468) ***/
    /*
    checkCudaErrors(cudaMemcpy(functionSolution3D, d_functionSolution3D, (NUMBER_OF_TIME_STEPS + 1) * TRIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost));
    for (int i = 0; i <= NUMBER_OF_TIME_STEPS; i++) {
      printf("functionSolution3D(x%d, y%d, z%d) = [%.7f, %.7f, %.7f]\n", i, i, i,
      functionSolution3D[i * TRIDIMENSIONAL_VALUE + 0],
      functionSolution3D[i * TRIDIMENSIONAL_VALUE + 1],
      functionSolution3D[i * TRIDIMENSIONAL_VALUE + 2]);
    }*/
    /* End Print Corrector Convolution Computation (OK) */
  }
  
  checkCudaErrors(cudaMemcpy(functionSolution3D, d_functionSolution3D, (NUMBER_OF_TIME_STEPS + 1) * BIDIMENSIONAL_VALUE * sizeof(double), cudaMemcpyDeviceToHost));
  /*for (int i = 0; i <= NUMBER_OF_TIME_STEPS; i++) {
    printf("functionSolution3D(x%d, y%d, z%d) = [%.7f, %.7f, %.7f]\n", i, i, i,
      functionSolution3D[i * TRIDIMENSIONAL_VALUE + 0],
      functionSolution3D[i * TRIDIMENSIONAL_VALUE + 1],
	  functionSolution3D[i * TRIDIMENSIONAL_VALUE + 2]);
  }*/
  
  


  /* Stop measurement */
  cudaEventRecord(cudaStop, 0);
  cudaEventSynchronize(cudaStop);
  float milliseconds = 0;
  cudaDeviceSynchronize();
  cudaEventElapsedTime(&milliseconds, cudaStart, cudaStop);

  clock_t cpuEnd = clock();

  printf("DONE !\n");
  printf("CPU Total ellapsed time is %.6f ms\n", (float) (cpuEnd - cpuStart) / CLOCKS_PER_SEC * 1000);
  printf("GPU Total elapsed time is %.6f ms\n", milliseconds);

  fprintf(fisierOut, "Timp executie kernel (NUMBER_OF_TIME_STEPS = %d): %.6f ms / %.6f s / %.6f min \n", NUMBER_OF_TIME_STEPS, milliseconds, milliseconds / 1000, milliseconds/60000);
  
  for (int i = 0; i <= NUMBER_OF_TIME_STEPS; i++) {
	  fprintf(fisierOut, "functionSolution3D(x%d, y%d) = [%.7f, %.7f]\n", i, i,
		  functionSolution3D[i * BIDIMENSIONAL_VALUE + 0],
		  functionSolution3D[i * BIDIMENSIONAL_VALUE + 1]);
		  //functionSolution3D[i * BIDIMENSIONAL_VALUE + 2]);
  }
  
  getchar();

  return 0;
}

__global__ void convolutionKernel(double *inputValues3D, double *outputValues3D, int currentTimeStep) {
  int columnID = blockIdx.x * blockDim.x + threadIdx.x;

  convolutionFunction3D(inputValues3D, outputValues3D, currentTimeStep, columnID);
}

__global__ void coefficientsKernel(double *predictorInputVectorB, double *correctorInputVectorC, double *correctorInputVectorA) {
  int columnID = blockIdx.x * blockDim.x + threadIdx.x;

  if (columnID <= NUMBER_OF_TIME_STEPS) {
    /* Compute an */
    *(correctorInputVectorA + columnID) = pow((double) columnID + 2, ALPHA_VALUE + 1) - 2 * pow((double) columnID + 1, ALPHA_VALUE + 1) + pow((double) columnID, ALPHA_VALUE + 1);
    /* Compute bn */
    *(predictorInputVectorB + columnID) = pow((double) columnID + 1, ALPHA_VALUE) - pow((double) columnID, ALPHA_VALUE);
    /* Compute cn */
    *(correctorInputVectorC + columnID) = pow((double) columnID, ALPHA_VALUE + 1) - (columnID - ALPHA_VALUE) * pow((double) columnID + 1, ALPHA_VALUE);
  }
}

/* (1, N) x (N, 3) = (1, 3), where 3 = Convolution Function Dimension (x, y, z) */
__global__ void vectorMatrixMultiplication3D(double *vectorInputValue1, double *vectorInputValue2, double *matrixInputValue, double *outputSumVector3D1, double *outputSumVector3D2, int currentTimeStep) {
  int rowID = blockIdx.y * blockDim.y + threadIdx.y;
  if (rowID < currentTimeStep) {
    //printf("threadIdx.x = %d, blockIdx.x = %d\n", threadIdx.x, blockIdx.x);
    /* Example : Input Vector = [b0, b1, b2, b3] */
    /* Example : Input Matrix = [{f0x, f0y, f0z}, {f1x, f1y, f1z}, {f2x, f2y, f2z}, {f3x, f3y, f3z}] */
    /* Example : Output Vector = [b3 * f0x + b2 * f1x + b1 * f2x + b0 * f3x, b3 * f0y + b2 * f1y + b1 * f2y + b0 * f3y, b3 * f0z + b2 * f1z + b1 * f2z + b0 * f3z] */
    /**
    * Reverse rowID with threadIdx.x in order to place the partial products related to the same sum one after another, instead of one on top of the other.
    * Equivalent with obtaining a column-wise positioning instead of row-wise (coallesced access for sum reduce performance).
    */
    *(outputSumVector3D1 + threadIdx.x * NUMBER_OF_TIME_STEPS + rowID) = *(vectorInputValue1 + currentTimeStep - 1 - rowID) * (*(matrixInputValue + rowID * BIDIMENSIONAL_VALUE + threadIdx.x));
    *(outputSumVector3D2 + threadIdx.x * NUMBER_OF_TIME_STEPS + rowID) = *(vectorInputValue2 + currentTimeStep - 1 - rowID) * (*(matrixInputValue + rowID * BIDIMENSIONAL_VALUE + threadIdx.x));
  }
}

__global__ void intermediatePredictorKernel(const double predictorGammaTerm, double *initialConditions3D, double *predictor3D) {
  int columnID = blockIdx.x * blockDim.x + threadIdx.x;

  /* Compute h ^ q / Gamma(q + 1) * s1 */
  constantVectorMultiplication3D(predictorGammaTerm, predictor3D, columnID);
  /* Compute v0 + h ^ q / Gamma(q + 1) * s1 */
  vectorAddition3D(predictor3D, initialConditions3D, columnID);
}

__global__ void intermediateCorrectorKernel(const double correctorGammaTerm, double *predictor3D, double *convolutedPredictor3D, double *initialConditions3D, double *corrector3D, double *functionSolution3D, int currentTimeStep) {
  int columnID = blockIdx.x * blockDim.x + threadIdx.x;

  /* Compute f[p] */
  convolutionFunction3D(predictor3D, convolutedPredictor3D, 0, columnID);
  /* Compute f[p] + s2 */
  vectorAddition3D(corrector3D, convolutedPredictor3D, columnID);
  /* Compute h ^ q / Gamma(q + 2) * (f[p] + s2) */
  constantVectorMultiplication3D(correctorGammaTerm, corrector3D, columnID);
  /* Compute v0 + h ^ q / Gamma(q + 2) * (f[p] + s2) */
  vectorAddition3D(corrector3D, initialConditions3D, columnID);
  /**
    Be sure that corrector3D is ready before calling convolutionFunction3D.
    This is necessary because all its values are used to compute a specific dimension of functionSolution3D.
  */
  __syncthreads();
  /* Compute fsol = f[y] */
  convolutionFunction3D(corrector3D, functionSolution3D, currentTimeStep, columnID);
}

__global__ void sumReduceKernel(double *predictor3DPartialProducts, double *corrector3DPartialProducts, double *predictor3D, double *corrector3D, size_t dynamicSize, int finalSum) {
  /**
   * Shared Memory for faster Sum Reduce kernel.
  */
  extern __shared__ double sharedPartialProducts[];

  /**
   * First half of the Shared Memory, belongs to predictor3DPartialProducts computation.
   * Second half of the Shared Memory, belongs to corrector3DPartialProducts computation.
  */
  double *sharedPredictor3DPartialProducts = sharedPartialProducts;
  double *sharedCorrector3DPartialProducts = (double *) &sharedPartialProducts[dynamicSize];

  unsigned int sharedPartialProductsOffset = threadIdx.y * blockDim.x + threadIdx.x;
  unsigned int partialProductsOffset;
  
  //if (!collocatedOffset) 
    partialProductsOffset = threadIdx.y * NUMBER_OF_TIME_STEPS + threadIdx.x + blockIdx.x * blockDim.x;
  //else
  //  partialProductsOffset = threadIdx.y * blockDim.x + threadIdx.x;
  /*
  if (gridDim.x == 1 && blockDim.x <= 256) {
    partialProductsOffset = threadIdx.y * blockDim.x + threadIdx.x;
  }
  else {
    partialProductsOffset = threadIdx.y * NUMBER_OF_TIME_STEPS + threadIdx.x;
  }
  */

  /**
   * Each thread is responsible for initializing its element corresponding to predictor3DPartialProducts computation [threadIdx.x].
   * Each thread is responsible for initializing its element corresponding to corrector3DPartialProducts computation [threadIdx.x + dynamicSize].
  */
  if ((threadIdx.x + blockIdx.x * blockDim.x) < NUMBER_OF_TIME_STEPS) {
      sharedPredictor3DPartialProducts[sharedPartialProductsOffset] = predictor3DPartialProducts[partialProductsOffset];
      sharedCorrector3DPartialProducts[sharedPartialProductsOffset] = corrector3DPartialProducts[partialProductsOffset];
  }
  else {
    sharedPartialProducts[sharedPartialProductsOffset] = 0;
    sharedPartialProducts[sharedPartialProductsOffset + dynamicSize] = 0;
  }

  /**
   * Threads must be synchronized to ensure that at this point, the Shared Memory is fully initialized.
  */
  __syncthreads();

  for (unsigned int i = blockDim.x / 2; i > 0; i >>= 1) {
    if (threadIdx.x < i) {
      sharedPredictor3DPartialProducts[sharedPartialProductsOffset] += sharedPredictor3DPartialProducts[sharedPartialProductsOffset + i];
      sharedCorrector3DPartialProducts[sharedPartialProductsOffset] += sharedCorrector3DPartialProducts[sharedPartialProductsOffset + i];
    }
    __syncthreads();
  }

  if (threadIdx.x == 0) {
    unsigned int resultIndex;
    if (finalSum)
      resultIndex = threadIdx.y;
    else
      resultIndex = threadIdx.y * NUMBER_OF_TIME_STEPS + blockIdx.x;
    predictor3D[resultIndex] = sharedPredictor3DPartialProducts[sharedPartialProductsOffset];
    corrector3D[resultIndex] = sharedCorrector3DPartialProducts[sharedPartialProductsOffset];
  }
}

__device__ void convolutionFunction3D(double *inputValues3D, double *outputValues3D, int currentTimeStep, int columnID) {
    double x = *(inputValues3D + 0);
	double y = *(inputValues3D + 1);
	//double z = *(inputValues3D + 2);

    if (columnID == 0) {
		*(outputValues3D + currentTimeStep * BIDIMENSIONAL_VALUE + columnID) = y - gFunction(x);
		//*(outputValues3D + currentTimeStep * BIDIMENSIONAL_VALUE + columnID) = y - A_VALUE * pow(x, 3) + B_VALUE * pow(x, 2) + IP_VALUE; //- z;
    }
    else if (columnID == 1) {
      //*(outputValues3D + currentTimeStep * BIDIMENSIONAL_VALUE + columnID) = C_VALUE - D_VALUE * pow(x, 2) - y;
	  *(outputValues3D + currentTimeStep * BIDIMENSIONAL_VALUE + columnID) = -BE_VALUE * (y + GA_VALUE * y + x) + FC_VALUE * sin(OM_VALUE * T_VALUE);
    }
	/*
    else {
      *(outputValues3D + currentTimeStep * BIDIMENSIONAL_VALUE + columnID) = EPS_VALUE * (S_VALUE * (x - M_VALUE) - z);
    }
	*/
}

__device__ double gFunction(double inputValue) {
	if (inputValue <= -1) {
		return B_VALUE * inputValue - A_VALUE + B_VALUE;
	}
	else if (inputValue >= 1) {
		return B_VALUE * inputValue + A_VALUE - B_VALUE;
	}
	else {
		return A_VALUE * inputValue;
	}
}

/* V1 = V1 + V2 */
__device__ void vectorAddition3D(double *firstInputOutputValues3D, double *secondInputValues3D, int columnID) {
  *(firstInputOutputValues3D + columnID) += *(secondInputValues3D + columnID);
}

/* V = x * V */
__device__ void constantVectorMultiplication3D(const double inputConstantValue, double *outputValues3D, int columnID) {
  *(outputValues3D + columnID) *= inputConstantValue;
}
