import math as mt
import numpy as np
from decimal import Decimal
import time
import numba as nmb
from accelerate import *

ac =  1. #np.float64(1)
bc = 3.
cc = 1.
dc = 5.
pc = (bc - dc)/ac
eps = 0.005
sc = 4.
Ip = np.float64(3.25)
x1 = np.float64(-1.618033988749895)
def f(vect):
    #print("vect:", vect)
    lst =[vect[1] - ac * (vect[0]**3) + bc * (vect[0]**2) + Ip - vect[2],
          cc - dc * (vect[0]**2) - vect[1],
          eps * (sc * (vect[0] - x1) - vect[2])]
    return lst

def dot(V, x): #{a, b, c} . {x, y, z} = a x + b y + c z
    return [x*e for e in V]
    
def sdot(v1, v2): #{a, b, c} . {x, y, z} = a x + b y + c z
    s = 0
    size = len(v1)
    for i in range(0, size):
        s = s + v1[i] * v2[i]
    return s

def mvdot(M, V): #{{a, b}, {c, d}} . {x, y} = {a x + b y, c x + d y}
    ret = []
    sizeM = len(M)
    sizeV = len(V)
    for i in range(0, sizeM):
        s = 0
        for j in range(0, sizeV): 
            s = s + M[i][j] * V[j]
        ret.append(s)
    return ret

def vmdot(V, M): #{x, y} . {{a, b}, {c, d}} = {a x + c y, b x + d y}
    ret = []
    sizeM = len(M[0])
    sizeV = len(V)
    for i in range(0, sizeM):
        s = 0
        for j in range(0, sizeV): 
            s = s + M[j][i] * V[j]
        ret.append(s)
    return ret

@cuda.cuda.jit(argtypes=[nmb.float32[:],
                                    nmb.float32[:],
                                    nmb.float32[:,:],
                                    nmb.uint32])
def cu_vmdot(s1, V, M, step): #{x, y} . {{a, b}, {c, d}} = {a x + c y, b x + d y}
    startX, startY, startZ = cuda.cuda.grid(3)
    gridX = cuda.cuda.gridDim.x * cuda.cuda.blockDim.x;
    gridY = cuda.cuda.gridDim.y * cuda.cuda.blockDim.y;
    gridZ = cuda.cuda.gridDim.z * cuda.cuda.blockDim.z;
    gridXYZ = gridX * gridY * gridZ

    cudaRuns = step / gridXYZ

    for k in range(0, cudaRuns):
        #for i in range(0, 3):
        ix = (startX*gridY+startY)*gridZ+startZ
        IDx = k * gridXYZ + ix
        if IDx <= step:
            s1[0] = s1[0] + M[IDx][0] * V[IDx]
            s1[1] = s1[1] + M[IDx][1] * V[IDx]
            s1[2] = s1[2] + M[IDx][2] * V[IDx]



def solution(q, h):
    v0 = [0.1, 0.1, 0.1]
    sol[0] = v0
    fsol[0] = f(v0)
    #b = []
    #c = []
    #c1 = []
    for j in range(1, Range):#int(Num)+1):
        #print("_____________")
        #print(j)
        for k in range(1, j):
            b[k] = b[k-1]
        b[0] = np.float64(j**q - (j-1)**q)
        #print("b:", b)
        #s1 = vmdot(b, fsol)

        d_fsol = cuda.cuda.to_device(fsol)
        d_s1 = cuda.cuda.to_device(s1)
        d_b = cuda.cuda.to_device(b)
        cu_vmdot[griddim, blockdim](d_s1, d_b, d_fsol, j)
        d_s1.to_host()
        print("s1:",s1)
        k = j-1
        '''
    if j >= 2:
            c.insert(0, np.float64((k + 1)**(q + 1) - 2*(k**(q + 1)) + (k - 1)**(q + 1)))

        c1 = list(c)
        c1.insert(0, (j-1)**(q + 1) - (j-1-q)*(j**q))
        '''      
        #print("c:", c)
        #print("c1:", c1)
        
        #s2 = vmdot(c1, fsol)
        #print("s2:", s2)
        s2 = s1
        s11 = dot(s1, ((h**q) / mt.gamma(q+1)))
        #print("s11", s11)
        
        p = np.add(v0, s11)
        #print("p:",p)
        
        fs2 = np.add(f(list(p)), s2)
        s22 = dot(fs2, ((h**q) / mt.gamma(q+2)))
        y = list(np.add(v0, s22))
        #print("y:", y)

        sol[j] = y
        
        fsol[j] = f(y)

        
    #print("sol:", sol)
    #print("fsol:", fsol)
        
h = np.float64(0.01)
Tmax = 30
Num = Tmax/h
q = np.float64(0.8)
#fsol = []
#sol = []
blockdim = (2, 3, 4)
griddim = (5, 6, 7)
Range = 5040 * 2 #6*7 grid-uri * 3*4 block-uri * 100
sol = np.zeros((Range, 3), dtype = np.float32)
fsol = np.zeros((Range, 3), dtype = np.float32)
s1 = np.zeros((3), dtype = np.float32)
#fsol[0] = f(sol[0])
b = np.zeros((Range), dtype = np.float32)
c = np.zeros((Range), dtype = np.float64)
c1 = np.zeros((Range), dtype = np.float64)

def main():
    #print(mt.gamma(q+1))
    #print(mt.gamma(q+2))
    start = time.time()
    solution(q, h)
    end = time.time()
    print("s:",end-start)

    name = "outFsolTest"+str(time.time())+".txt"
    f = open(name, "w")
    header = "Fsol[" + str(len(fsol)) +"][" + str(len(fsol[0])) + "] ends in " + str(end-start) + " s\n"
    f.write(header)    
    for e in fsol:
        line = "[" + str(e[0]) +  ", " + str(e[1]) + ", " + str(e[2]) + "]\n"
        f.write(line)

    f.close()
    
if __name__ == "__main__":
    main()
