"""
h = 0.01
Tmax = 30000
Num = Tmax/h  3000000
q = 0.8

def f([x,y,z]):

	a =  1.
	b = 3.
	c = 1.
	d = 5.
	p = (b - d)/a
	eps = 0.005
	s = 4.
	I = 3.25
	x1 = -1.618033988749895
    #print("vect:", vect)
    	xx = y - a * (x^3) + b * (y^2) + I - z
	yy = c - d * (x^2) - y
     zz = eps * (s * (x - x1) - z)
    
	return [xx,yy,zz]

def solution(q, h):
    xyz = [0.1, 0.1, 0.1]
    sol = [xyz]
    fsol = [f(xyz)]

    B = []
    C = []
    C1 = []
    for j in range(1, int(Num)+1):
        
        B[j-1] = (j^q - (j-1)^q))
        
        S1 =  fsol . B //convolutia partiala 

        k = j-1
        if j >= 2:
            C[j-1] = ((k + 1)^(q + 1) - 2*(k^(q + 1)) + (k - 1)^(q + 1)))

        C1 = C
        c1 adaugi (k)^(q + 1) - (k-q)*(j^q))
        
        S2 = C1 . fsol // alta convolutie
        S11 = S1 *  ((h**q) / mt.gamma(q+1)) // fiecare element din S1 se inmulteste cu asta
        
        
        vp = v0 + s11 // astia is vectori toti
      
        newSol = v0 + [f(vp) + S2]  * ((h**q) / mt.gamma(q+2))) // tot produs scalar
  
        sol[j] = newSol
        
        fsol[j] = f(newSol)
        
"""


import math as mt
import numpy as np
from decimal import Decimal
import time

ac =  1.
bc = 3.
cc = 1.
dc = 5.
pc = (bc - dc)/ac
eps = 0.005
sc = 4.
Ip = 3.25
x1 = -1.618033988749895
def f(vect):
    #print("vect:", vect)
    lst =[vect[1] - ac * (vect[0]**3) + bc * (vect[0]**2) + Ip - vect[2],
          cc - dc * (vect[0]**2) - vect[1],
          eps * (sc * (vect[0] - x1) - vect[2])]
    return lst

def dot(V, x): #{a, b, c} . {x, y, z} = a x + b y + c z
    return [x*e for e in V]
    
def sdot(v1, v2): #{a, b, c} . {x, y, z} = a x + b y + c z
    s = 0
    size = len(v1)
    for i in range(0, size):
        s = s + v1[i] * v2[i]
    return s

def mvdot(M, V): #{{a, b}, {c, d}} . {x, y} = {a x + b y, c x + d y}
    ret = []
    sizeM = len(M)
    sizeV = len(V)
    for i in range(0, sizeM):
        s = 0
        for j in range(0, sizeV): 
            s = s + M[i][j] * V[j]
        ret.append(s)
    return ret

def vmdot(V, M): #{x, y} . {{a, b}, {c, d}} = {a x + c y, b x + d y}
    ret = []
    sizeM = len(M[0])
    sizeV = len(V)
    for i in range(0, sizeM):
        s = 0
        for j in range(0, sizeV): 
            s = s + M[j][i] * V[j]
        ret.append(s)
    return ret

def solution(q, h):
    v0 = [0.1, 0.1, 0.1]
    print(v0)
    sol = [v0]
    print(sol)
    fsol = [f(v0)]
    print(fsol)
    b = []
    c = []
    c1 = []
    for j in range(1, int(Num)+1):
        print("_____________")
        print(j)
        b.insert(0, np.float64(j**q - (j-1)**q))
        print("b:", b)
        s1 = vmdot(b, fsol)
        print("s1:",s1)
        k = j-1
        if j >= 2:
            c.insert(0, np.float64((k + 1)**(q + 1) - 2*(k**(q + 1)) + (k - 1)**(q + 1)))

        c1 = list(c)
        c1.insert(0, (j-1)**(q + 1) - (j-1-q)*(j**q))
        
        print("c:", c)
        print("c1:", c1)
        
        s2 = vmdot(c1, fsol)
        print("s2:", s2)
        s11 = dot(s1, ((h**q) / mt.gamma(q+1)))
        print("s11", s11)
        
        p = np.add(v0, s11)
        print("p:",p)
        
        fs2 = np.add(f(list(p)), s2)
        s22 = dot(fs2, ((h**q) / mt.gamma(q+2)))
        y = list(np.add(v0, s22))
        print("y:", y)

        sol.append(y)
        
        fsol.append(f(y))
        
        print("sol:", sol)
        print("fsol:", fsol)
        
h = np.float64(0.01)
Tmax = 0.3
Num = Tmax/h
q = np.float64(0.8)

def main():
    #print(mt.gamma(q+1))
    #print(mt.gamma(q+2))
    start = time.time()
    solution(q, h)
    end = time.time()
    print("s:",end-start)

if __name__ == "__main__":
    main()
