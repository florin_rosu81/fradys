#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <time.h>
#include <math.h>
#include <complex.h>

#include <fftw3-mpi.h>

#define MPI_TAG_IN 42
#define MPI_TAG_OUT 43

static const double alpha = 0.5;

void convolution_fftw_exchange_in(fftw_complex *f, ptrdiff_t ni,
                                  MPI_Comm mpicomm)
{
    int mpisize;
    int mpirank;
    MPI_Status status;
    ptrdiff_t n = ni / 2;

    MPI_Comm_size(mpicomm, &mpisize);
    MPI_Comm_rank(mpicomm, &mpirank);

    /* send this half to its rightful process */
    if (mpirank != 0) {
        MPI_Send(f, n, MPI_C_DOUBLE_COMPLEX, mpirank / 2, MPI_TAG_IN, mpicomm);
    }

    /* receive two halves from other processes */
    if (mpirank >= (mpisize / 2)) {
        memset (f, 0, ni * sizeof(fftw_complex));
    } else {
        if (mpirank != 0) {
            MPI_Recv(f, n, MPI_C_DOUBLE_COMPLEX, 2 * mpirank, MPI_TAG_IN, mpicomm, &status);
        }
        MPI_Recv(f + n, n, MPI_C_DOUBLE_COMPLEX, 2 * mpirank + 1, MPI_TAG_IN, mpicomm, &status);
    }
}

void convolution_fftw_exchange_out(fftw_complex *h, ptrdiff_t ni,
                                   MPI_Comm mpicomm)
{
    int mpisize;
    int mpirank;
    MPI_Status status;
    ptrdiff_t n = ni / 2;

    MPI_Comm_size(mpicomm, &mpisize);
    MPI_Comm_rank(mpicomm, &mpirank);

    if (mpirank < (mpisize / 2)) {
        if (mpirank != 0) {
            MPI_Send(h, n, MPI_C_DOUBLE_COMPLEX, 2 * mpirank, MPI_TAG_OUT, mpicomm);
        }
        MPI_Send(h + n, n, MPI_C_DOUBLE_COMPLEX, 2 * mpirank + 1, MPI_TAG_OUT, mpicomm);
    }

    if (mpirank != 0) {
        MPI_Recv(h, n, MPI_C_DOUBLE_COMPLEX, mpirank / 2, MPI_TAG_OUT, mpicomm, &status);
    }
}

void convolution_fftw (MPI_Comm mpicomm, ptrdiff_t n)
{
    const double gamma0 = tgamma (alpha + 1);
    int mpisize;
    int mpirank;

    fftw_plan forward;
    fftw_plan backward;

    ptrdiff_t local_alloc;
    ptrdiff_t ni, si;   /* input size and start */
    ptrdiff_t no, so;   /* output size and start */

    fftw_complex *f;
    fftw_complex *g;
    fftw_complex *h;
    fftw_complex *fhat;
    fftw_complex *ghat;
    fftw_complex *hhat;

    /* get MPI info */
    MPI_Comm_size (mpicomm, &mpisize);
    MPI_Comm_rank (mpicomm, &mpirank);

    /* get size for local arrays */
    local_alloc = fftw_mpi_local_size_1d(2 * n, mpicomm, FFTW_FORWARD,
                                         FFTW_ESTIMATE, &ni, &si, &no, &so);
#ifndef NDEBUG
    printf ("[%d] local_alloc %td\n", mpirank, local_alloc);
    printf ("[%d] ni %td si %td\n", mpirank, ni, si);
    printf ("[%d] no %td so %td\n", mpirank, no, so);
#endif

    /* allocate arrays */
    f = fftw_alloc_complex (local_alloc);
    g = fftw_alloc_complex (local_alloc);
    h = fftw_alloc_complex (local_alloc);
    fhat = fftw_alloc_complex (local_alloc);
    ghat = fftw_alloc_complex (local_alloc);
    hhat = fftw_alloc_complex (local_alloc);

    /* create plans */
    forward = fftw_mpi_plan_dft_1d (2 * n, f, fhat, mpicomm,
                                    FFTW_FORWARD, FFTW_ESTIMATE);
    backward = fftw_mpi_plan_dft_1d (2 * n, hhat, h, mpicomm,
                                     FFTW_BACKWARD, FFTW_ESTIMATE);

    double time = (double) clock();
    /* fill in half of the array, the half we know */
    for (ptrdiff_t i = si / 2; i < (si + ni) / 2; ++i) {
        f[i - si / 2] = (pow (i + 1, alpha) - pow (i, alpha)) / gamma0 + 0 * I;
        g[i - si / 2] = exp (-(i * 0.00001)) + 0 * I;
    }

    /* perform the exchanges */
    convolution_fftw_exchange_in(f, ni, mpicomm);
    convolution_fftw_exchange_in(g, ni, mpicomm);

    /* perform the transformation */
    fftw_mpi_execute_dft (forward, f, fhat);
    fftw_mpi_execute_dft (forward, g, ghat);

    /* element by element product */
    for (ptrdiff_t i = 0; i < ni; ++i) {
        hhat[i] = fhat[i] * ghat[i] / (2 * n);
    }

    /* inverse transformation for the result => convolution */
    fftw_mpi_execute_dft (backward, hhat, h);
    convolution_fftw_exchange_out(h, ni, mpicomm);
    printf ("[%d] convolution_fftw time %gs\n", mpirank, ((double) clock() - time) / CLOCKS_PER_SEC);

    fftw_destroy_plan(forward);
    fftw_destroy_plan(backward);

    fftw_free(f);
    fftw_free(g);
    fftw_free(h);
    fftw_free(fhat);
    fftw_free(ghat);
    fftw_free(hhat);
}

int main (int argc, char **argv)
{
    MPI_Comm mpicomm = MPI_COMM_WORLD;
    const ptrdiff_t n = 65536;

    MPI_Init (&argc, &argv);
    fftw_mpi_init ();

    convolution_fftw (mpicomm, n);

    fftw_mpi_cleanup();
    MPI_Finalize ();

    return EXIT_SUCCESS;
}
