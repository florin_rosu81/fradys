#include <stdio.h>
#include <stdlib.h>

#include <time.h>
#include <math.h>

#include <omp.h>
#include <complex.h>
#include <fftw3.h>

void convolution_openmp (fftw_complex *f, fftw_complex *g, fftw_complex *h, size_t N)
{
#pragma omp parallel for
    for (size_t n = 1; n < N + 1; ++n) {
        h[n - 1] = 0;
        for (size_t j = 0; j < n; ++j) {
            h[n - 1] += f[n - j - 1] * g[j];
        }
    }
}

void convolution_fftw (fftw_complex *f, fftw_complex *g, fftw_complex *h, size_t N)
{
    fftw_plan forward;
    fftw_plan backward;

    fftw_complex *fhat;
    fftw_complex *ghat;
    fftw_complex *hhat;

    fhat = fftw_alloc_complex (N);
    ghat = fftw_alloc_complex (N);
    hhat = fftw_alloc_complex (N);

    forward = fftw_plan_dft_1d (N, f, fhat, FFTW_FORWARD, FFTW_ESTIMATE);
    backward = fftw_plan_dft_1d (N, hhat, h, FFTW_BACKWARD, FFTW_ESTIMATE);

    fftw_execute_dft (forward, f, fhat);
    fftw_execute_dft (forward, g, ghat);

    for (size_t i = 0; i < N; ++i) {
        hhat[i] = fhat[i] * ghat[i] / N;
    }

    fftw_execute_dft (backward, hhat, h);

    fftw_free (fhat);
    fftw_free (ghat);
    fftw_free (hhat);

    fftw_destroy_plan (forward);
    fftw_destroy_plan (backward);
}

int main (void)
{
    const size_t n = 32;
    const double alpha = 0.5;
    const double gamma0 = tgamma (alpha + 1);

    fftw_complex *f;
    fftw_complex *g;
    fftw_complex *h1;
    fftw_complex *h2;
    double time;

    f = fftw_alloc_complex(2 * n);
    g = fftw_alloc_complex(2 * n);
    h1 = fftw_alloc_complex(2 * n);
    h2 = fftw_alloc_complex(2 * n);

    for (size_t i = 0; i < n; ++i) {
        f[i] = (pow (i + 1, alpha) - pow (i, alpha)) / gamma0;
        f[i + n] = 0.0;
        g[i] = exp (-(i * 0.00001));
        g[i + n] = 0.0;
        h1[i] = h2[i] = 0.0;
    }
    for (size_t i = 0; i < 2 * n; ++i) {
        printf("%10g %10g\n", creal(f[i]), creal(g[i]));
    }

    time = (double) clock ();
    convolution_openmp (f, g, h1, n);
    printf ("convolution_openmp: %gs\n", ((double) clock() - time) / CLOCKS_PER_SEC);

    fftw_init_threads ();
    fftw_plan_with_nthreads (omp_get_max_threads ());

    time = (double) clock ();
    convolution_fftw (f, g, h2, 2 * n);
    printf ("convolution_fftw: %gs\n", ((double) clock() - time) / CLOCKS_PER_SEC);

    fftw_cleanup_threads ();

    printf("%10s %10s\n", "omp", "fftw");
    for (size_t i = 0; i < n; ++i) {
        printf("%10g %10g\n", creal(h1[i]), creal(h2[i]));
    }

    fftw_free (f);
    fftw_free (g);
    fftw_free (h1);
    fftw_free (h2);

    return EXIT_SUCCESS;
}
