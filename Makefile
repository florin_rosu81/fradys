# App
TARGET=fractional
CFLAGS= -g -Wall -Wextra -std=c99 -pedantic
CC=mpicc

# Files
OBJ=	src/utils.o \
	src/interpolation.o \
	src/fractional.o \
	src/workspace.o
HEADERS= src/utils.h src/interpolation.h src/fractional.h src/workspace.h

# External Libraries
LIB_DIR= -L/usr/lib
LIB_M= -lm
LIB_FFTW= -lfftw3 -lfftw3_mpi
LIB_GSL= -lgsl
LIB_CBLAS= -lcblas
LIBS= ${LIB_M} ${LIB_FFTW} ${LIB_GSL} ${LIB_CBLAS}
DEFINES= -DHAVE_GSL -DNDEBUG

all: ${TARGET}

%.o: %.c
	${CC} ${CFLAGS} ${DEFINES} -c $^ -o $@

${TARGET}: ${OBJ} main.o
	${CC} ${CFLAGS} ${DEFINES} ${HEADERS} ${OBJ} main.o -o ${TARGET} ${LIBS}

clean:
	@rm -rf *.o *~ ${TARGET}

.PHONY: all clean
