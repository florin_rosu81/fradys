# Compilation

To compile with CMake, execute the following commands in the top folder:
```
mkdir build
cd build
cmake ..
make
```
